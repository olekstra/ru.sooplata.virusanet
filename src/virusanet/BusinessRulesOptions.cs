﻿namespace Olekstra.VirusaNet
{
    using System;

    public class BusinessRulesOptions
    {
        public string[] ValidQualificationTexts { get; set; }

        public AllowedCardStep[] AllowedCardSteps { get; set; }

        public int TimeoutSinceFirstTransaction { get; set; }

        public class AllowedCardStep
        {
            public string Name { get; set; }

            public decimal Price { get; set; }
        }
    }
}
