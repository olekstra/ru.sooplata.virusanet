﻿namespace Olekstra.VirusaNet.Public.ViewModels
{
    public class ResponseResult
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public static ResponseResult Success(string message)
        {
            return new ResponseResult
            {
                Status = true,
                Message = message
            };
        }

        public static ResponseResult Fail(string message)
        {
            return new ResponseResult
            {
                Status = false,
                Message = message
            };
        }
    }
}
