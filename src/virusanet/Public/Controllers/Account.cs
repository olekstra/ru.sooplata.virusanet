﻿namespace Olekstra.VirusaNet.Public.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.Options;
    using Model;

    [Authorize]
    public class Account : Controller
    {
        private UserManager<User> userManager;
        private SignInManager<User> signInManager;
        private IAuthorizationService authorizationService;
        private VirusaNetDb db;
        private string externalCookieScheme;

        public Account(
            VirusaNetDb db,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IAuthorizationService authorizationService,
            IOptions<IdentityCookieOptions> identityCookieOptions)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.authorizationService = authorizationService;
            this.db = db;
            this.externalCookieScheme = identityCookieOptions.Value.ExternalCookieAuthenticationScheme;
        }

        #region Routes

        public static UrlActionContext LoginRoute(string returnUrl = null)
        {
            return new UrlActionContext()
            {
                Action = nameof(Login),
                Controller = nameof(Account),
                Values = new { Area = string.Empty, returnUrl = returnUrl }
            };
        }

        public static UrlActionContext LoginCompleteRoute(string returnUrl = null)
        {
            return new UrlActionContext()
            {
                Action = nameof(LoginComplete),
                Controller = nameof(Account),
                Values = new { Area = string.Empty, returnUrl = returnUrl }
            };
        }

        public static UrlActionContext AccessDeniedRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(AccessDenied),
                Controller = nameof(Account),
                Values = new { Area = string.Empty }
            };
        }

        public static UrlActionContext ExternalLoginCallbackRoute(string returnUrl = null)
        {
            return new UrlActionContext()
            {
                Action = nameof(ExternalLoginCallback),
                Controller = nameof(Account),
                Values = new { Area = string.Empty, returnUrl = returnUrl }
            };
        }

        public static UrlActionContext LogoutRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Logout),
                Controller = nameof(Account),
                Values = new { Area = string.Empty }
            };
        }

        #endregion

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(returnUrl);
            }

            await HttpContext.Authentication.SignOutAsync(externalCookieScheme);

            ViewBag.ReturnUrl = returnUrl;
            ViewBag.LoginProviders = signInManager.GetExternalAuthenticationSchemes().ToList();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string login, string passwd, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(returnUrl);
            }

            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(passwd))
            {
                return View();
            }

            var result = await signInManager.PasswordSignInAsync(login, passwd, true, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                // прямой сейчас authorizationService еще не готов делать авторизацию,
                // надо обновить страницу чтобы фильтры в начале сработали и подхватили аутентификацию
                // поэтому редирект на другую страницу, а там уже сработает проверка в начале метода контроллера
                return this.Redirect(LoginCompleteRoute(returnUrl));
            }

            if (result.IsLockedOut)
            {
                return View("AccessDenied");
            }

            ModelState.AddModelError(string.Empty, "Указанные имя и пароль не найдены");
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult LoginComplete(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(returnUrl);
            }

            return this.Redirect(LoginRoute(returnUrl));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return this.Redirect(LoginRoute());
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            var redirectUrl = Url.Action(ExternalLoginCallbackRoute(returnUrl));
            var properties = signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null)
        {
            var info = await signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return this.Redirect(LoginRoute());
            }

            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            var user = await userManager.FindByEmailAsync(email);
            if (user == null || user.IsArchived)
            {
                await signInManager.SignOutAsync();
                return this.Redirect(AccessDeniedRoute());
            }

            await signInManager.SignInAsync(user, isPersistent: true);
            return RedirectToLocal(returnUrl);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (authorizationService.AuthorizeAsync(User, null, Startup.PolicyNameDrugstore).Result)
            {
                if (returnUrl != null && returnUrl.StartsWith("/apteka"))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return this.Redirect(Apteka.Controllers.Home.IndexRoute());
                }
            }

            if (returnUrl != null && returnUrl.StartsWith("/admin"))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return this.Redirect(Admin.Controllers.Home.IndexRoute());
            }
        }
    }
}