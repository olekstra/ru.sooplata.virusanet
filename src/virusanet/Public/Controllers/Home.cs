﻿namespace Olekstra.VirusaNet.Public.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Antiforgery;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Services;
    using ViewModels;

    [AllowAnonymous]
    public class Home : Controller
    {
        private QualificationService qualificationService;
        private IAntiforgery antiforgery;
        private BusinessRulesOptions options;
        private IFileStorageService storageService;
        private ILogger logger;

        public Home(QualificationService qualificationService, IAntiforgery antiforgery, IOptions<BusinessRulesOptions> options, IFileStorageService storageService, ILogger<Home> logger)
        {
            this.qualificationService = qualificationService;
            this.antiforgery = antiforgery;
            this.options = options.Value;
            this.storageService = storageService;
            this.logger = logger;
        }

        public static UrlActionContext AboutRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(About),
                Controller = nameof(Home),
                Values = new { Area = string.Empty }
            };
        }

        [HttpGet("about")]
        public async Task<IActionResult> About([FromServices] VirusaNetDb db)
        {
            var model = new List<Tuple<string, string>>();
            try
            {
                model.Add(Tuple.Create("DB Test", (await db.Transactions.CountAsync()).ToString()));
                model.Add(Tuple.Create("MONITORING", "TYR65DMREUMTY6MYTXEC"));
            }
            catch (Exception ex)
            {
                model.Add(Tuple.Create("Exception", ex.Message));
            }

            return View(model);
        }

        [Route("/")]
        [Route("/index")]
        public IActionResult Index()
        {
            return File("index-nJk6lyDOyhm0gONyhWd2.html", "text/html");
        }

        [Route("/token")]
        public IActionResult GetValidationToken()
        {
            var tokenSet = antiforgery.GetAndStoreTokens(HttpContext);
            return Json(new { name = tokenSet.FormFieldName, value = tokenSet.RequestToken });
        }

        [Route("/qualPatterns")]
        public IActionResult GetQualificationPatterns()
        {
            return Json(options.ValidQualificationTexts);
        }

        [Route("/qualify")]
        [Route("/qualify/{text}")]
        public IActionResult Qualify(string text)
        {
            Response.Cookies.Append("search", text, new CookieOptions { Expires = DateTimeOffset.Now.AddMonths(1) });
            logger.LogDebug($"Qualify() - '{text}' saved to cookie, redirecting to registration.");
            return Redirect("/register#reg");
        }

        [Route("/register")]
        public IActionResult Register(string search)
        {
            if (string.IsNullOrEmpty(search))
            {
                logger.LogDebug($"Register() - search param is empty, reading from cookie.");
                search = Request.Cookies["search"];
            }

            if (qualificationService.Validate(search))
            {
                return File("registration-KUkH1E7pu8gpOFIl3RYP.html", "text/html");
            }

            logger.LogError($"Register(): search value '{search}' is invalid. Redirecting to index page.");
            return Redirect("/");
        }

        [HttpPost]
        [Route("/reg1")]
        public async Task<IActionResult> Reg1(
            string card,
            string phone,
            bool agree,
            [FromServices] CardRegistrationService registrationService)
        {
            if (string.IsNullOrWhiteSpace(card))
            {
                return Json(new ResponseResult { Status = false, Message = "Не указан номер листовки" });
            }

            if (string.IsNullOrWhiteSpace(phone))
            {
                return Json(new ResponseResult { Status = false, Message = "Не указан номер телефона" });
            }

            if (!agree)
            {
                return Json(new ResponseResult { Status = false, Message = "Необходимо согласиться с условиями программы" });
            }

            PhoneNumber phoneNumber;
            if (!PhoneNumber.TryParse(phone, out phoneNumber))
            {
                return Json(new ResponseResult { Status = false, Message = "Номер телефона указан неверно" });
            }

            if (!Card.IsValidId(card))
            {
                return Json(new ResponseResult { Status = false, Message = "Номер листовки указан неверно" });
            }

            var result = await registrationService.RegStep1Async(card, phoneNumber);

            logger.LogDebug($"Результат первого шага регистрации: {result.Status} {result.Message}");

            return Json(result);
        }

        [HttpPost]
        [Route("/reg2")]
        public async Task<IActionResult> Reg2(
            string card,
            string phone,
            string phonecode,
            bool agree,
            [FromServices] CardRegistrationService registrationService)
        {
            if (string.IsNullOrWhiteSpace(card))
            {
                return Json(new ResponseResult { Status = false, Message = "Не указан номер листовки" });
            }

            if (string.IsNullOrWhiteSpace(phone))
            {
                return Json(new ResponseResult { Status = false, Message = "Не указан номер телефона" });
            }

            if (string.IsNullOrWhiteSpace(phonecode))
            {
                return Json(new ResponseResult { Status = false, Message = "Не указан контрольный код" });
            }

            if (!agree)
            {
                return Json(new ResponseResult { Status = false, Message = "Необходимо согласиться с условиями программы" });
            }

            var file1 = Request.Form.Files["passport"];
            if (file1 == null || file1.Length == 0)
            {
                return Json(new ResponseResult { Status = false, Message = "Необходимо приложить скан/фото разворота паспорта с фотографией" });
            }

            var file2 = Request.Form.Files["recipe"];
            if (file2 == null || file2.Length == 0)
            {
                return Json(new ResponseResult { Status = false, Message = "Необходимо приложить скан/фото документа, подтверждающего назначение препарата" });
            }

            PhoneNumber phoneNumber;
            if (!PhoneNumber.TryParse(phone, out phoneNumber))
            {
                return Json(new ResponseResult { Status = false, Message = "Номер телефона указан неверно" });
            }

            if (!Card.IsValidId(card))
            {
                return Json(new ResponseResult { Status = false, Message = "Номер листовки указан неверно" });
            }

            var result = await registrationService.RegStep2Async(card, phoneNumber, phonecode);

            logger.LogDebug($"Результат второго шага регистрации: {result.Status} {result.Message}");

            // если всё плохо, то сразу возвращаем результат
            if (!result.Status)
            {
                return Json(result);
            }

            // сохраняем приложеные файлы
            await storageService.SaveCardScan(card, "passport.jpg", file1.OpenReadStream());
            await storageService.SaveCardScan(card, "recipe.jpg", file2.OpenReadStream());

            return Json(result);
        }
    }
}
