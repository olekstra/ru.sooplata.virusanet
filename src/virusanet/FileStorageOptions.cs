﻿namespace Olekstra.VirusaNet
{
    public class FileStorageOptions
    {
        public string TransactionFolder { get; set; }

        public string CardFolder { get; set; }
    }
}
