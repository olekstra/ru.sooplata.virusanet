﻿namespace Olekstra.VirusaNet.Apteka.ViewModels
{
    using System;
    using Model;

    public class TransactionConfirmViewModel
    {
        public Guid Id { get; set; }

        public PhoneNumber Phone { get; set; }

        public string ConfirmationCode { get; set; }

        public BusinessRulesOptions.AllowedCardStep Step { get; set; }
    }
}
