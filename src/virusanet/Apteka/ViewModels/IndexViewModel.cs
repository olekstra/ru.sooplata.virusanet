﻿namespace Olekstra.VirusaNet.Apteka.ViewModels
{
    using System;
    using System.Collections.Generic;

    using Model;

    public class IndexViewModel
    {
        public string Phone { get; set; }

        public bool PhoneNumberNotParsed { get; set; }

        public List<Transaction> CompletedTransactions { get; set; }
    }
}
