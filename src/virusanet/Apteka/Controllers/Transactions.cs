﻿namespace Olekstra.VirusaNet.Apteka.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Services;
    using ViewModels;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameDrugstore)]
    public class Transactions : Controller
    {
        public const string AreaName = Home.AreaName;

        private const string ControllerName = nameof(Transactions);

        private VirusaNetDb db;

        private IRetailService retailService;

        private BusinessRulesOptions businessRulesOptions;

        private UserManager<User> userManager;

        private ILogger logger;

        private IFileStorageService fileStorage;

        public Transactions(VirusaNetDb db, IRetailService retailService, UserManager<User> userManager, IOptions<BusinessRulesOptions> businessRulesOptions, ILogger<Transactions> logger, IFileStorageService fileStorage)
        {
            this.db = db;
            this.retailService = retailService;
            this.userManager = userManager;
            this.businessRulesOptions = businessRulesOptions.Value;
            this.logger = logger;
            this.fileStorage = fileStorage;
        }

        #region Routing

        public static UrlActionContext ConfirmRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Confirm),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext CompleteRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Complete),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext CompletedOkRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(CompletedOk),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext AbortedOkRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(AbortedOk),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext AddScanRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(AddScan),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext IndexRoute(DateTime? from, DateTime? to)
        {
            return new UrlActionContext
            {
                 Controller = ControllerName,
                 Action = nameof(Index),
                 Values = new { Area = AreaName, from = from, to = to }
            };
        }

        public static UrlActionContext CompletedListRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(CompletedList),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }
        #endregion

        public async Task<IActionResult> Confirm(Guid id)
        {
            var trans = await db.Transactions.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (trans == null)
            {
                return NotFound();
            }

            var drugstoreId = User.GetDrugstoreId();
            if (trans.DrugstoreId != drugstoreId)
            {
                logger.LogCritical($"Не совпадает drugstoreId: текущий {drugstoreId}, в транзакции {trans.DrugstoreId}");
                throw new Exception("DrugstoreId mismatch");
            }

            if (trans.Status != TransactionStatus.Started)
            {
                throw new NotImplementedException("Wrong status: " + trans.Status);
            }

            var model = new TransactionConfirmViewModel();
            model.Id = trans.Id;
            model.Phone = new PhoneNumber((await db.Cards.SingleAsync(x => x.Id == trans.CardId)).Phone);
            model.ConfirmationCode = string.Empty;
            model.Step = businessRulesOptions.AllowedCardSteps[trans.StepNumber - 1];

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Confirm(Guid id, TransactionConfirmViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            var trans = await db.Transactions.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (trans == null)
            {
                return NotFound();
            }

            var drugstoreId = User.GetDrugstoreId();
            if (trans.DrugstoreId != drugstoreId)
            {
                logger.LogCritical($"Не совпадает drugstoreId: текущий {drugstoreId}, в транзакции {trans.DrugstoreId}");
                throw new Exception("DrugstoreId mismatch");
            }

            var result = await retailService.ConfirmTransactionAsync(id, model.ConfirmationCode, Guid.Parse(userManager.GetUserId(User)));

            if (!result)
            {
                ModelState.AddModelError(string.Empty, "Неверный код подтверждения");

                model.Phone = new PhoneNumber((await db.Cards.SingleAsync(x => x.Id == trans.CardId)).Phone);
                model.Step = businessRulesOptions.AllowedCardSteps[trans.StepNumber - 1];

                return View(model);
            }

            logger.LogInformation($"Транзакция {model.Id} была успешно подтверждена.");

            return this.Redirect(CompleteRoute(id));
        }

        public async Task<IActionResult> Complete(Guid id)
        {
            var trans = await db.Transactions.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (trans == null)
            {
                return NotFound();
            }

            var drugstoreId = User.GetDrugstoreId();
            if (trans.DrugstoreId != drugstoreId)
            {
                logger.LogCritical($"Не совпадает drugstoreId: текущий {drugstoreId}, в транзакции {trans.DrugstoreId}");
                throw new Exception("DrugstoreId mismatch");
            }

            switch (trans.Status)
            {
                case TransactionStatus.Confirmed:
                    var model = businessRulesOptions.AllowedCardSteps[trans.StepNumber - 1];
                    return View(model);
                case TransactionStatus.Completed:
                    return this.Redirect(CompletedOkRoute());
                case TransactionStatus.Aborted:
                    return this.Redirect(AbortedOkRoute());
                default:
                    throw new NotImplementedException("Wrong status: " + trans.Status);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Complete(Guid id, string success)
        {
            var trans = await db.Transactions.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (trans == null)
            {
                return NotFound();
            }

            var drugstoreId = User.GetDrugstoreId();
            if (trans.DrugstoreId != drugstoreId)
            {
                logger.LogCritical($"Не совпадает drugstoreId: текущий {drugstoreId}, в транзакции {trans.DrugstoreId}");
                throw new Exception("DrugstoreId mismatch");
            }

            var userId = Guid.Parse(userManager.GetUserId(User));

            switch (trans.Status)
            {
                case TransactionStatus.Confirmed:
                case TransactionStatus.Completed:
                case TransactionStatus.Aborted:
                    break;
                default:
                    throw new NotImplementedException("Wrong status: " + trans.Status);
            }

            if (success == "1")
            {
                switch (trans.Status)
                {
                    case TransactionStatus.Confirmed:
                        await retailService.CompleteTransactionAsync(id, userId);
                        return this.Redirect(CompletedOkRoute());
                    case TransactionStatus.Completed:
                        return this.Redirect(CompletedOkRoute());
                    default:
                        throw new NotImplementedException("Wrong status: " + trans.Status);
                }
            }

            if (success == "2")
            {
                switch (trans.Status)
                {
                    case TransactionStatus.Confirmed:
                        await retailService.AbortTransactionAsync(id, userId);
                        return this.Redirect(AbortedOkRoute());
                    case TransactionStatus.Aborted:
                        return this.Redirect(AbortedOkRoute());
                    default:
                        throw new NotImplementedException("Wrong status: " + trans.Status);
                }
            }

            throw new Exception("Unknown success: " + success);
        }

        public IActionResult CompletedOk()
        {
            return View();
        }

        public IActionResult AbortedOk()
        {
            return View();
        }

        public async Task<IActionResult> Index(DateTime? from, DateTime? to)
        {
            if (!from.HasValue || !to.HasValue)
            {
                var nowDate = DateTimeOffset.Now.Date;
                to = nowDate;
                from = nowDate.AddMonths(-1);
            }

            if (from > to)
            {
                ModelState.AddModelError(nameof(to), "Дата конца не может быть меньше даты начала");
            }

            ViewBag.From = from;
            ViewBag.To = to;

            if (!ModelState.IsValid)
            {
                return View(new List<Transaction>());
            }

            var fromDate = new DateTimeOffset(from.Value.Year, from.Value.Month, from.Value.Day, 0, 0, 0, TimeSpan.Zero);
            var toDate = new DateTimeOffset(to.Value.Year, to.Value.Month, to.Value.Day, 0, 0, 0, TimeSpan.Zero).AddDays(1);

            var drugstoreId = Guid.Parse(User.FindFirst("DrugstoreId").Value);

            var tran = await db.Transactions.AsNoTracking()
                .Where(x => x.StartTime >= fromDate && x.StartTime < toDate)
                .Where(x => x.DrugstoreId == drugstoreId)
                .OrderByDescending(x => x.StartTime)
                .ToListAsync();

            return View(tran);
        }

        public async Task<IActionResult> CompletedList()
        {
            var drugstoreId = Guid.Parse(User.FindFirst("DrugstoreId").Value);
            var transactions = await db.Transactions.AsNoTracking()
                .Where(x => x.Status == TransactionStatus.Completed || x.Status == TransactionStatus.AwaitingValidation)
                .Where(x => x.DrugstoreId == drugstoreId)
                .OrderByDescending(x => x.StartTime)
                .ToListAsync();

            return View(transactions);
        }

        public async Task<IActionResult> AddScan(Guid id)
        {
            var tran = await db.Transactions.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (tran == null)
            {
                return NotFound();
            }

            var drugstoreId = User.GetDrugstoreId();
            if (tran.DrugstoreId != drugstoreId)
            {
                logger.LogCritical($"Не совпадает drugstoreId: текущий {drugstoreId}, в транзакции {tran.DrugstoreId}");
                throw new Exception("DrugstoreId mismatch");
            }

            if (tran.Status != TransactionStatus.Completed && tran.Status != TransactionStatus.AwaitingValidation)
            {
                throw new NotImplementedException("Wrong status: " + tran.Status);
            }

            return View(tran);
        }

        [HttpPost]
        [ActionName("AddScan")]
        public async Task<IActionResult> AddScanPost(Guid id)
        {
            var tran = await db.Transactions.FirstOrDefaultAsync(x => x.Id == id);

            if (tran == null)
            {
                return NotFound();
            }

            if (tran.Status != TransactionStatus.Completed && tran.Status != TransactionStatus.AwaitingValidation)
            {
                throw new NotImplementedException("Wrong status: " + tran.Status);
            }

            var file = Request.Form.Files["file"];
            if (file == null || file.Length == 0)
            {
                ModelState.AddModelError("file", "Не указан файл для загрузки");

                return View(tran);
            }

            if (!file.FileName.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase) && !file.FileName.EndsWith(".png", StringComparison.OrdinalIgnoreCase) && !file.FileName.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase))
            {
                ModelState.AddModelError("file format", "Не верный формат файла");

                return View(tran);
            }

            string fileName = await fileStorage.SaveTransactionScan(id, file.FileName, file.OpenReadStream());

            tran.ScanFileName = fileName;
            tran.Status = TransactionStatus.AwaitingValidation;

            db.Entry<Transaction>(tran).State = EntityState.Modified;

            var tranHistory = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                TransactionId = tran.Id,
                Time = DateTimeOffset.Now,
                UserId = Guid.Parse(userManager.GetUserId(User)),
                Comment = string.Format("Добавлен скан чека {0}", fileName)
            };

            db.TransactionHistories.Add(tranHistory);

            db.SaveChanges();

            return this.Redirect(IndexRoute(null, null));
        }
    }
}
