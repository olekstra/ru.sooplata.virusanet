﻿namespace Olekstra.VirusaNet.Apteka.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Model;
    using ViewModels;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameDrugstore)]
    public class Home : Controller
    {
        public const string AreaName = "apteka";

        private const string ControllerName = nameof(Home);

        private VirusaNetDb db;

        private IRetailService retailService;

        private UserManager<User> userManager;

        private ILogger logger;

        public Home(VirusaNetDb db, IRetailService retailService, UserManager<User> userManager, ILogger<Home> logger)
        {
            this.db = db;
            this.retailService = retailService;
            this.userManager = userManager;
            this.logger = logger;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Index),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext RejectPhoneRoute(string phone)
        {
            return new UrlActionContext()
            {
                Action = nameof(RejectPhone),
                Controller = ControllerName,
                Values = new { Area = AreaName, phone = phone }
            };
        }

        #endregion

        public async Task<IActionResult> Index()
        {
            var model = new IndexViewModel()
            {
                CompletedTransactions = await GetCompletedTransactionsAsync()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(string phone = null)
        {
            var model = new IndexViewModel()
            {
                Phone = phone
            };

            if (string.IsNullOrWhiteSpace(phone))
            {
                model.CompletedTransactions = await GetCompletedTransactionsAsync();
                return View(model);
            }

            PhoneNumber phoneNumber;
            if (!PhoneNumber.TryParse(phone, out phoneNumber))
            {
                logger.LogWarning($"Номер телефона указан неверно: '{phone}'");
                ModelState.AddModelError(nameof(phone), "Номер телефона указан неправильно");

                model.PhoneNumberNotParsed = true;
                model.CompletedTransactions = await GetCompletedTransactionsAsync();
                return View(model);
            }

            logger.LogDebug($"Пробую начать транзакцию для телефона {phoneNumber}");
            var trans = await retailService.BeginTransactionAsync(phoneNumber, GetCurrentUserDrugstoreId(), Guid.Parse(userManager.GetUserId(User)));

            if (trans == null)
            {
                return this.Redirect(RejectPhoneRoute(phoneNumber.ToBeautyPhone()));
            }

            return this.Redirect(Transactions.ConfirmRoute(trans.Id));
        }

        public IActionResult RejectPhone(string phone)
        {
            ViewBag.Phone = phone;
            return View();
        }

        private Guid GetCurrentUserDrugstoreId()
        {
            return Guid.Parse(User.FindFirst("DrugstoreId").Value);
        }

        private Task<List<Transaction>> GetCompletedTransactionsAsync()
        {
            var drugstoreId = GetCurrentUserDrugstoreId();
            return db.Transactions.AsNoTracking()
                .Where(x => x.Status == TransactionStatus.Completed)
                .Where(x => x.DrugstoreId == drugstoreId)
                .OrderByDescending(x => x.StartTime)
                .ToListAsync();
        }
    }
}
