﻿namespace Olekstra.VirusaNet.Apteka
{
    using System;
    using System.Security.Claims;

    public static class UserExtensions
    {
        public static Guid GetDrugstoreId(this ClaimsPrincipal user)
        {
            return Guid.Parse(user.FindFirst("DrugstoreId").Value);
        }
    }
}
