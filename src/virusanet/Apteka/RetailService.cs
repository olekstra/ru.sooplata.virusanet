﻿namespace Olekstra.VirusaNet.Apteka
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Services;

    public class RetailService : IRetailService
    {
        private static readonly Random Random = new Random();

        private VirusaNetDb db;
        private BusinessRulesOptions businessRulesOptions;
        private IMessageService messageService;
        private ILogger<RetailService> logger;

        public RetailService(VirusaNetDb db, IOptions<BusinessRulesOptions> businessRulesOptions, IMessageService messageService, ILogger<RetailService> logger)
        {
            this.db = db;
            this.businessRulesOptions = businessRulesOptions.Value;
            this.messageService = messageService;
            this.logger = logger;
        }

        /// <summary>
        /// Проверяет, что за данным номером телефона есть активная листовка,
        /// и начинает транзакцию по этой листовке
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public async Task<Transaction> BeginTransactionAsync(PhoneNumber phone, Guid drugstoreId, Guid userId)
        {
            logger.LogDebug($"BeginTransactionAsync({phone}, {drugstoreId}) начинается...");

            var drugstore = await db.Drugstores.AsNoTracking().SingleOrDefaultAsync(x => x.Id == drugstoreId);
            if (drugstore == null)
            {
                throw new Exception($"Аптека с указанным id не найдена: {drugstoreId}");
            }

            var nextStep = await GetNextCardStepAsync(phone);
            if (nextStep == null)
            {
                return null;
            }

            var trans = new Transaction
            {
                Id = Guid.NewGuid(),
                CardId = nextStep.Item1.Id,
                DrugstoreId = drugstoreId,
                StartTime = DateTimeOffset.Now,
                StepNumber = nextStep.Item2.StepNumber,
                Status = TransactionStatus.Started,
                ConfirmationCode = Random.Next(100000, 999999).ToString()
            };
            db.Transactions.Add(trans);

            var transHistory = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                TransactionId = trans.Id,
                Time = DateTimeOffset.Now,
                UserId = userId,
                Comment = "Телефон проверен, продажа начата"
            };
            db.TransactionHistories.Add(transHistory);

            nextStep.Item2.TransactionId = trans.Id;
            db.Entry(nextStep.Item2).State = EntityState.Modified;

            var cardMessage = $"Код подтверждения {trans.ConfirmationCode} для продажи {businessRulesOptions.AllowedCardSteps[trans.StepNumber - 1].Name}";

            // Сначала отсылаем код, потом сохраняем данные
            //   Если что-то пойдет не так, то лучше два сообщения держателю,
            //     чем "впустую потратить" его продажу
            await messageService.SendAsync(phone, cardMessage);
            await db.SaveChangesAsync();

            return trans;
        }

        public async Task<Tuple<Card, CardStep>> GetNextCardStepAsync(PhoneNumber phone)
        {
            var activeCards = await db.Cards.AsNoTracking()
                .Where(x => x.Phone == phone.ToOlekstraPhone())
                .Where(Card.ActiveCardExpression)
                .OrderBy(x => x.Id)
                .ToArrayAsync();

            if (activeCards.Length == 0)
            {
                logger.LogWarning($"За телефоном {phone} нет ни одной активной карты.");
                return null;
            }

            var activeCardIds = activeCards.Select(x => x.Id).ToArray();

            // Наверное тут будет "неоптимально" когда будет много карт одновременно и много шагов на одну карту
            var cardSteps = await db.CardSteps.AsNoTracking()
                .Where(x => activeCardIds.Contains(x.CardId))
                .ToListAsync();

            var now = DateTimeOffset.Now;
            foreach (var card in activeCards)
            {
                logger.LogDebug($"Проверяю карту {card.Id}...");
                var steps = cardSteps.Where(x => x.CardId == card.Id).OrderBy(x => x.StepNumber);
                var nextStep = steps.FirstOrDefault(x => x.OpenTime < now && x.TransactionId == null);

                if (nextStep != null)
                {
                    logger.LogInformation($"Найден следующий шаг для карты {card.Id}: {nextStep.StepNumber} (открылся {nextStep.OpenTime})");
                    return Tuple.Create(card, nextStep);
                }
            }

            logger.LogWarning($"Не найдено разрешенных операций ни по одной карте для телефона {phone} :(");
            return null;
        }

        public async Task<bool> ConfirmTransactionAsync(Guid id, string confirmationCode, Guid userId)
        {
            logger.LogDebug($"ConfirmTransactionAsync({id}) начинается...");

            var trans = await db.Transactions.SingleOrDefaultAsync(x => x.Id == id);
            if (trans == null)
            {
                throw new Exception($"Транзакция с указанным id не найдена: {id}");
            }

            if (trans.Status != TransactionStatus.Started)
            {
                throw new Exception($"Транзакция {id} имеет недопустимый статус: {trans.Status}");
            }

            var transHistory = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                TransactionId = trans.Id,
                Time = DateTimeOffset.Now,
                UserId = userId,
                //// Comment = ...
            };
            db.TransactionHistories.Add(transHistory);

            if (trans.ConfirmationCode == confirmationCode)
            {
                trans.Status = TransactionStatus.Confirmed;
                transHistory.Comment = "Введён правильный контрольный код";

                await db.SaveChangesAsync();

                return true;
            }
            else
            {
                transHistory.Comment = $"Введён неправильный контрольный код ({confirmationCode})";

                await db.SaveChangesAsync();

                return false;
            }
        }

        public async Task CompleteTransactionAsync(Guid id, Guid userId)
        {
            logger.LogDebug($"CompleteTransactionAsync({id}) начинается...");

            var trans = await db.Transactions.SingleOrDefaultAsync(x => x.Id == id);
            if (trans == null)
            {
                throw new Exception($"Транзакция с указанным id не найдена: {id}");
            }

            if (trans.Status != TransactionStatus.Confirmed)
            {
                throw new Exception($"Транзакция {id} имеет недопустимый статус: {trans.Status}");
            }

            trans.Status = TransactionStatus.Completed;

            var transHistory = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                TransactionId = trans.Id,
                Time = DateTimeOffset.Now,
                UserId = userId,
                Comment = "Продажа успешно выполнена"
            };
            db.TransactionHistories.Add(transHistory);

            await db.SaveChangesAsync();
        }

        public async Task AbortTransactionAsync(Guid id, Guid userId)
        {
            logger.LogDebug($"AbortTransactionAsync({id}) начинается...");

            var trans = await db.Transactions.SingleOrDefaultAsync(x => x.Id == id);
            if (trans == null)
            {
                throw new Exception($"Транзакция с указанным id не найдена: {id}");
            }

            if (trans.Status != TransactionStatus.Confirmed)
            {
                throw new Exception($"Транзакция {id} имеет недопустимый статус: {trans.Status}");
            }

            var cardStep = await db.CardSteps.SingleAsync(x => x.TransactionId == id);
            if (cardStep.CardId != trans.CardId || cardStep.StepNumber != trans.StepNumber)
            {
                throw new Exception("Не совпадают контрольные проверки");
            }

            // "освобождаем" CardStep
            cardStep.TransactionId = null;

            // откатываем транзакцию
            trans.Status = TransactionStatus.Aborted;

            var transHistory = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                TransactionId = trans.Id,
                Time = DateTimeOffset.Now,
                UserId = userId,
                Comment = "Продажа была отменена на последнем шаге"
            };
            db.TransactionHistories.Add(transHistory);

            await db.SaveChangesAsync();
        }
    }
}
