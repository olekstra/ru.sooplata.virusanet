﻿namespace Olekstra.VirusaNet.Apteka
{
    using System;
    using System.Threading.Tasks;

    using Model;

    public interface IRetailService
    {
        Task<Transaction> BeginTransactionAsync(PhoneNumber phone, Guid drugstoreId, Guid userId);

        Task<bool> ConfirmTransactionAsync(Guid id, string confirmationCode, Guid userId);

        Task CompleteTransactionAsync(Guid id, Guid userId);

        Task AbortTransactionAsync(Guid id, Guid userId);
    }
}
