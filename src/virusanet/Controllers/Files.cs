﻿namespace Olekstra.VirusaNet.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.Logging;
    using Olekstra.VirusaNet.Model;
    using Olekstra.VirusaNet.Services;

    [Authorize]
    public class Files : Controller
    {
        private const string AreaName = "";

        private const string ControllerName = nameof(Files);

        private VirusaNetDb db;

        private ILogger logger;

        private IFileStorageService fileStorage;

        private IAuthorizationService authorizationService;

        public Files(VirusaNetDb db, ILogger<Files> logger, IFileStorageService fileStorage, IAuthorizationService authorizationService)
        {
            this.db = db;
            this.logger = logger;
            this.fileStorage = fileStorage;
            this.authorizationService = authorizationService;
        }

        #region Routing
        public static UrlActionContext GetImageRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(GetScan),
                Controller = ControllerName,
                Values = new { area = AreaName, id = id }
            };
        }

        public static UrlActionContext GetImageRoute(Guid id, string fileName)
        {
            return new UrlActionContext()
            {
                Action = nameof(GetScan),
                Controller = ControllerName,
                Values = new { area = AreaName, id = id, fileName = fileName }
            };
        }
        #endregion

        public async Task<IActionResult> GetScan(Guid id, string fileName = "")
        {
            var tran = db.Transactions.FirstOrDefault(x => x.Id == id);
            if (tran == null)
            {
                return null;
            }

            var isAdmin = await authorizationService.AuthorizeAsync(User, null, Startup.PolicyNameAdmin);
            var isManager = await authorizationService.AuthorizeAsync(User, null, Startup.PolicyNameManager);
            var isDrugstore = await authorizationService.AuthorizeAsync(User, null, Startup.PolicyNameDrugstore);
            var drugstoreId = isDrugstore ? Guid.Parse(User.FindFirst("DrugstoreId").Value) : Guid.Empty;

            if (!isAdmin && !isManager && tran.DrugstoreId != drugstoreId)
            {
                return NotFound();
            }

            if (string.IsNullOrEmpty(tran.ScanFileName))
            {
                return NotFound();
            }

            var file = fileStorage.GetTransactionScan(tran.Id, fileName == string.Empty ? tran.ScanFileName : fileName);

            return File(file, GetContentType(tran.ScanFileName));
        }

        private string GetContentType(string fileName)
        {
            if (fileName.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) || fileName.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase))
            {
                return "image/jpeg";
            }

            if (fileName.EndsWith(".png", StringComparison.OrdinalIgnoreCase))
            {
                return "image/png";
            }

            return string.Empty;
        }
    }
}
