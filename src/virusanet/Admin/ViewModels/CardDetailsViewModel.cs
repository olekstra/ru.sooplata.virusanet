﻿namespace Olekstra.VirusaNet.Admin.ViewModels
{
    using System;
    using System.Collections.Generic;
    using Model;

    public class CardDetailsViewModel
    {
        public string Query { get; set; }

        public string Message { get; set; }

        public Card Card { get; set; }

        public List<CardStep> CardSteps { get; set; }

        public List<Transaction> Transactions { get; set; }

        public List<CardHistory> CardHistories { get; set; }
    }
}
