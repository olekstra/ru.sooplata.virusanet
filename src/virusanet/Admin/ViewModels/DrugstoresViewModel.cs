﻿namespace Olekstra.VirusaNet.Admin.ViewModels
{
    using System.Collections.Generic;
    using Olekstra.VirusaNet.Model;

    public class DrugstoresViewModel : Drugstore
    {
        public DrugstoresViewModel()
        {
            Users = new List<User>();
        }

        public DrugstoresViewModel(Drugstore drugstore)
        {
            this.Id = drugstore.Id;
            this.IsLocked = drugstore.IsLocked;
            this.Name = drugstore.Name;
            this.Timezone = drugstore.Timezone;
            this.Comment = drugstore.Comment;
            Users = new List<User>();
        }

        public List<User> Users { get; set; }
    }
}
