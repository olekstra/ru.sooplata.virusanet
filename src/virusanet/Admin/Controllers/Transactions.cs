﻿namespace Olekstra.VirusaNet.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Olekstra.VirusaNet.Admin.ViewModels;
    using Olekstra.VirusaNet.Model;
    using Services;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameManager)]
    public class Transactions : Controller
    {
        public const string Title = "Транзакции";
        public const string AreaName = Home.AreaName;
        private const string ControllerName = nameof(Transactions);

        private VirusaNetDb db;
        private ICardStepMoverService cardStepMoverService;
        private UserManager<User> userManager;
        private ILogger logger;
        private IFileStorageService fileService;

        public Transactions(VirusaNetDb db, ICardStepMoverService cardStepMoverService, UserManager<User> userManager, ILogger<Transactions> logger, IFileStorageService fileService)
        {
            this.db = db;
            this.cardStepMoverService = cardStepMoverService;
            this.userManager = userManager;
            this.logger = logger;
            this.fileService = fileService;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Index),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext IndexUnvalidatedRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(IndexUnvalidated),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext DetailsRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Details),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext ValidateRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Validate),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        #endregion

        public async Task<IActionResult> Index()
        {
            var list = await db.Transactions.AsNoTracking()
                .Include(x => x.Drugstore)
                .Include(x => x.Card)
                .OrderByDescending(x => x.StartTime)
                .Take(100)
                .ToListAsync();

            return View(list);
        }

        public async Task<IActionResult> IndexUnvalidated()
        {
            var list = await db.Transactions.AsNoTracking()
                .Include(x => x.Drugstore)
                .Include(x => x.Card)
                .Where(x => x.Status == TransactionStatus.AwaitingValidation)
                .OrderByDescending(x => x.StartTime)
                .ToListAsync();

            return View(list);
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var model = await db.Transactions.AsNoTracking()
                .Include(x => x.Drugstore)
                .Include(x => x.Card)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            ViewBag.Scans = fileService.GetAllTransactionFileNames(model.Id);

            ViewBag.History = await db.TransactionHistories.AsNoTracking()
                .Include(x => x.User)
                .OrderByDescending(x => x.Time)
                .Where(x => x.TransactionId == id)
                .ToListAsync();

            return View(model);
        }

        public async Task<IActionResult> Validate(Guid id)
        {
            var model = await db.Transactions.AsNoTracking()
                .Include(x => x.Drugstore)
                .Include(x => x.Card)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            if (model.Status != TransactionStatus.AwaitingValidation)
            {
                throw new Exception("Wrond Status: " + model.Status);
            }

            ViewBag.Scans = fileService.GetAllTransactionFileNames(model.Id);

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Validate(Guid id, TransactionStatus status, string comment)
        {
            var model = await db.Transactions.SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            if (model.Status != TransactionStatus.AwaitingValidation)
            {
                throw new Exception("Wrond Status: " + model.Status);
            }

            model.Status = status;

            var userId = Guid.Parse(userManager.GetUserId(User));

            db.TransactionHistories.Add(new TransactionHistory
            {
                Id = Guid.NewGuid(),
                TransactionId = id,
                Time = DateTimeOffset.Now,
                UserId = userId,
                Comment = "Статус: " + status.GetDescription() + "; " + Environment.NewLine + comment
            });

            await db.SaveChangesAsync();

            await cardStepMoverService.CheckCardAsync(model.CardId, userId);

            return this.Redirect(IndexUnvalidatedRoute());
        }
    }
}
