﻿namespace Olekstra.VirusaNet.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Model;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameAdmin)]
    public class CardImport : Controller
    {
        public const string Title = "Листовки";
        public const string AreaName = Home.AreaName;
        private const string ControllerName = nameof(CardImport);

        private VirusaNetDb db;
        private UserManager<User> userManager;
        private ILogger logger;

        public CardImport(VirusaNetDb db, UserManager<User> userManager, ILogger<CardImport> logger)
        {
            this.db = db;
            this.userManager = userManager;
            this.logger = logger;
        }

        #region Routing

        public static UrlActionContext ImportRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Import),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        #endregion

        public IActionResult Import()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Import(string numbers)
        {
            var ids = numbers.Split('\r', '\n').Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();

            var invalid = ids.Where(x => !Card.IsValidId(x)).ToList();
            if (invalid.Count != 0)
            {
                ModelState.AddModelError(string.Empty, $"Обнаружено {invalid.Count} недопустимых номеров листовок, например '{invalid[0]}'");
            }

            if (!ModelState.IsValid)
            {
                return View((object)numbers);
            }

            var existing = await db.Cards.Where(x => ids.Contains(x.Id)).ToListAsync();
            if (existing.Count != 0)
            {
                ModelState.AddModelError(string.Empty, $"Обнаружено {existing.Count} уже существующих листовок, например '{existing[0]}'");
            }

            if (!ModelState.IsValid)
            {
                return View((object)numbers);
            }

            logger.LogDebug($"Для создания листовок передано {ids.Count} номеров");

            var userId = Guid.Parse(userManager.GetUserId(User));

            var newCards = ids.Select(x => new Card { Id = x }).ToList();
            var newCardHistories = ids.Select(x => new CardHistory
            {
                Id = Guid.NewGuid(),
                CardId = x,
                Time = DateTimeOffset.Now,
                UserId = userId,
                Comment = "Листовка добавлена в систему"
            }).ToList();

            db.Cards.AddRange(newCards);
            db.CardHistories.AddRange(newCardHistories);
            await db.SaveChangesAsync();

            logger.LogInformation($"Успешно созданы {newCards.Count} листовок");

            return this.Redirect(Home.IndexRoute());
        }
    }
}
