﻿namespace Olekstra.VirusaNet.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameAdmin)]
    public class Users : Controller
    {
        public const string Title = "Пользователи";
        public const string AreaName = Home.AreaName;
        private const string ControllerName = nameof(Users);

        private VirusaNetDb db;

        public Users(VirusaNetDb db)
        {
            this.db = db;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Index),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Create),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Edit),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Delete),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        #endregion

        public async Task<IActionResult> Index()
        {
            var list = await db.Users.Where(x => x.DrugstoreId == null).OrderBy(x => x.Email).ToListAsync();
            return View(list);
        }

        public IActionResult Create()
        {
            var model = new User();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(User model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.NormalizedEmail = model.Email.ToUpperInvariant();
            model.NormalizedName = model.Name.ToUpperInvariant();
            model.SecurityStamp = Guid.NewGuid();

            db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            return this.Redirect(IndexRoute());
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var model = await db.Users.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            if (model.DrugstoreId.HasValue)
            {
                throw new Exception("Из этого раздела нельзя управлять пользователями аптек");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, User model)
        {
            var oldModel = await db.Users.SingleOrDefaultAsync(x => x.Id == id);
            if (oldModel == null)
            {
                return NotFound();
            }

            if (oldModel.DrugstoreId.HasValue)
            {
                throw new Exception("Из этого раздела нельзя управлять пользователями аптек");
            }

            if (model.Id != id)
            {
                ModelState.AddModelError(nameof(model.Id), "Идентификатор не совпадает. Обратитесь к разработчикам.");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            oldModel.Email = model.Email;
            oldModel.Name = model.Name;
            oldModel.IsAdmin = model.IsAdmin;
            oldModel.IsArchived = model.IsArchived;

            oldModel.NormalizedEmail = oldModel.Email.ToUpperInvariant();
            oldModel.NormalizedName = oldModel.Name.ToUpperInvariant();
            oldModel.SecurityStamp = Guid.NewGuid();

            await db.SaveChangesAsync();

            return this.Redirect(IndexRoute());
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var model = await db.Users.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            if (model.DrugstoreId.HasValue)
            {
                throw new Exception("Из этого раздела нельзя управлять пользователями аптек");
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.Users.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            if (model.DrugstoreId.HasValue)
            {
                throw new Exception("Из этого раздела нельзя управлять пользователями аптек");
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return this.Redirect(IndexRoute());
        }
    }
}
