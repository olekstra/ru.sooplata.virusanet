﻿namespace Olekstra.VirusaNet.Admin.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Model;
    using Services;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameManager)]
    public class Reports : Controller
    {
        public const string Title = "Отчеты";
        public const string AreaName = Home.AreaName;
        private const string ControllerName = nameof(Reports);

        private VirusaNetDb db;
        private IConfigurationRoot configuration;
        private ILogger logger;

        public Reports(VirusaNetDb db, IConfigurationRoot configuration, ILogger<CardImport> logger)
        {
            this.db = db;
            this.configuration = configuration;
            this.logger = logger;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Index),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext ReportByCardsRoute(string key = null)
        {
            return new UrlActionContext()
            {
                Action = nameof(ReportByCards),
                Controller = ControllerName,
                Values = new { Area = AreaName, key = key }
            };
        }

        public static UrlActionContext ReportByCardStepsRoute(string key = null)
        {
            return new UrlActionContext()
            {
                Action = nameof(ReportByCardSteps),
                Controller = ControllerName,
                Values = new { Area = AreaName, key = key }
            };
        }

        #endregion

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public Task<IActionResult> ReportByCards()
        {
            return GenerateReportByCards();
        }

        [AllowAnonymous]
        public Task<IActionResult> ReportByCards(string key)
        {
            if (!string.Equals(key, configuration["ExportKey"], StringComparison.OrdinalIgnoreCase))
            {
                return Task.FromResult((IActionResult)Forbid());
            }

            return GenerateReportByCards();
        }

        [HttpPost]
        public Task<IActionResult> ReportByCardSteps()
        {
            return GenerateReportByCardSteps();
        }

        [AllowAnonymous]
        public Task<IActionResult> ReportByCardSteps(string key)
        {
            if (!string.Equals(key, configuration["ExportKey"], StringComparison.OrdinalIgnoreCase))
            {
                return Task.FromResult((IActionResult)Forbid());
            }

            return GenerateReportByCardSteps();
        }

        private async Task<IActionResult> GenerateReportByCards()
        {
            var data = await db.Cards.AsNoTracking().OrderBy(x => x.Id).ToListAsync();

            var defaultTz = TimeZoneService.GetTimeZoneByIdOrDefault(TimeZoneService.DefaultTimezoneId);

            var ms = new System.IO.MemoryStream();
            using (var textWriter = new System.IO.StreamWriter(ms, System.Text.Encoding.UTF8, 2048, true))
            {
                var opts = new CsvHelper.Configuration.CsvConfiguration
                {
                    Delimiter = ";",
                    CultureInfo = new System.Globalization.CultureInfo("ru-RU")
                };

                using (var csvWriter = new CsvHelper.CsvWriter(textWriter, opts))
                {
                    csvWriter.WriteField("Номер");
                    csvWriter.WriteField("Статус");
                    csvWriter.WriteField("Дата регистрации");
                    csvWriter.WriteField("Телефон");
                    csvWriter.NextRecord();

                    foreach (var item in data)
                    {
                        csvWriter.WriteField(item.Id);
                        csvWriter.WriteField(item.GetStatusText());
                        if (item.RegTime.Year <= 1900)
                        {
                            csvWriter.WriteField(null);
                        }
                        else
                        {
                            csvWriter.WriteField(TimeZoneInfo.ConvertTime(item.RegTime, defaultTz).DateTime);
                        }

                        csvWriter.WriteField(item.Phone);
                        csvWriter.NextRecord();
                    }
                }
            }

            ms.Position = 0;
            return File(ms, "application/vnd.ms-excel", "report.csv");
        }

        private async Task<IActionResult> GenerateReportByCardSteps()
        {
            var data = await db.CardSteps.AsNoTracking()
                .Include(x => x.Card)
                .Include(x => x.Transaction).ThenInclude(t => t.Drugstore)
                .OrderBy(x => x.CardId)
                .ThenBy(x => x.StepNumber)
                .ToListAsync();

            var defaultTz = TimeZoneService.GetTimeZoneByIdOrDefault(TimeZoneService.DefaultTimezoneId);

            var ms = new System.IO.MemoryStream();
            using (var textWriter = new System.IO.StreamWriter(ms, System.Text.Encoding.UTF8, 2048, true))
            {
                var opts = new CsvHelper.Configuration.CsvConfiguration
                {
                    Delimiter = ";",
                    CultureInfo = new System.Globalization.CultureInfo("ru-RU")
                };

                using (var csvWriter = new CsvHelper.CsvWriter(textWriter, opts))
                {
                    csvWriter.WriteField("Номер листовки");
                    csvWriter.WriteField("Статус листовки");
                    csvWriter.WriteField("Дата регистрации листовки");
                    csvWriter.WriteField("Телефон листовки");
                    csvWriter.WriteField("Номер упаковки");
                    csvWriter.WriteField("Дата открытия");
                    csvWriter.WriteField("Статус покупки");
                    csvWriter.WriteField("Дата покупки");
                    csvWriter.WriteField("Контрольный код");
                    csvWriter.WriteField("Аптека");
                    csvWriter.WriteField("Аптека заблокирована?");
                    csvWriter.NextRecord();

                    foreach (var item in data)
                    {
                        csvWriter.WriteField(item.CardId);
                        csvWriter.WriteField(item.Card.GetStatusText());
                        if (item.Card.RegTime.Year <= 1900)
                        {
                            csvWriter.WriteField(null);
                        }
                        else
                        {
                            csvWriter.WriteField(TimeZoneInfo.ConvertTime(item.Card.RegTime, defaultTz).DateTime);
                        }

                        csvWriter.WriteField(item.Card.Phone);

                        csvWriter.WriteField(item.StepNumber);
                        csvWriter.WriteField(TimeZoneInfo.ConvertTime(item.OpenTime, defaultTz).DateTime);
                        if (item.TransactionId.HasValue)
                        {
                            if (item.Transaction.Status == TransactionStatus.Validated)
                            {
                                csvWriter.WriteField("Выполнена");
                            }
                            else if (item.Transaction.Status == TransactionStatus.AwaitingValidation)
                            {
                                csvWriter.WriteField("На проверке");
                            }
                            else
                            {
                                csvWriter.WriteField("В процессе");
                            }
                        }
                        else
                        {
                            csvWriter.WriteField("Доступна");
                        }

                        if (item.Transaction == null)
                        {
                            csvWriter.WriteField(null);
                            csvWriter.WriteField(null);
                            csvWriter.WriteField(null);
                            csvWriter.WriteField(null);
                        }
                        else
                        {
                            csvWriter.WriteField(TimeZoneInfo.ConvertTime(item.Transaction.StartTime, defaultTz).DateTime);
                            csvWriter.WriteField(item.Transaction.ConfirmationCode);
                            csvWriter.WriteField(item.Transaction.Drugstore.Name);
                            csvWriter.WriteField(item.Transaction.Drugstore.IsLocked);
                        }

                        csvWriter.NextRecord();
                    }
                }
            }

            ms.Position = 0;
            return File(ms, "application/vnd.ms-excel", "report.csv");
        }
    }
}
