﻿namespace Olekstra.VirusaNet.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameManager)]
    public class Home : Controller
    {
        public const string AreaName = "admin";
        private const string ControllerName = nameof(Home);

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Index),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext LogsRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Logs),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        #endregion

        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Policy = Startup.PolicyNameAdmin)]
        [HttpGet(AreaName + "/logs")]
        public IActionResult Logs()
        {
            return View(iflight.Logging.MemoryLogger.LogList);
        }
    }
}
