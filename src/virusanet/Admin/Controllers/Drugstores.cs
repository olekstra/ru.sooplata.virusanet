﻿namespace Olekstra.VirusaNet.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Olekstra.VirusaNet.Admin.ViewModels;
    using Olekstra.VirusaNet.Model;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameManager)]
    public class Drugstores : Controller
    {
        public const string Title = "Аптеки";
        public const string AreaName = Home.AreaName;
        private const string ControllerName = nameof(Drugstores);

        private VirusaNetDb db;
        private UserManager<User> userManager;

        public Drugstores(VirusaNetDb db, UserManager<User> userManager)
        {
            this.db = db;
            this.userManager = userManager;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Index),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(Create),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Edit),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Delete),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext DetailsRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Details),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext CreateUserRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(CreateUser),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext DeleteUserRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(DeleteUser),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext EditUserRoute(Guid id)
        {
            return new UrlActionContext()
            {
                Action = nameof(EditUser),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }
        #endregion

        public async Task<IActionResult> Index(string search)
        {
            List<DrugstoresViewModel> list = null;
            if (!string.IsNullOrEmpty(search))
            {
                ViewBag.Search = search;
                search = search.ToLower();
                list = await db.Drugstores.Where(x => x.Name.ToLower().Contains(search)).Select(d => new DrugstoresViewModel()
                {
                    Comment = d.Comment,
                    Id = d.Id,
                    IsLocked = d.IsLocked,
                    Name = d.Name,
                    Timezone = d.Timezone
                }).ToListAsync();

                ViewBag.NothingFound = list.Count() > 0 ? false : true;
            }
            else
            {
                list = await db.Drugstores
                    .Select(d => new DrugstoresViewModel()
                    {
                        Comment = d.Comment,
                        Id = d.Id,
                        IsLocked = d.IsLocked,
                        Name = d.Name,
                        Timezone = d.Timezone
                    }).ToListAsync();
            }

            for (var i = 0; i < list.Count(); i++)
            {
                list[i].Users = await db.Users.Where(u => u.DrugstoreId == list[i].Id).ToListAsync();
            }

            return View(list);
        }

        public IActionResult Create()
        {
            var model = new Drugstore();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Drugstore model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Added;

            await db.SaveChangesAsync();

            return this.Redirect(IndexRoute());
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var model = await db.Drugstores.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.Drugstores.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return this.Redirect(IndexRoute());
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var model = await db.Drugstores.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, Drugstore model)
        {
            var oldModel = await db.Drugstores.SingleOrDefaultAsync(x => x.Id == id);
            if (oldModel == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            oldModel.Name = model.Name;
            oldModel.IsLocked = model.IsLocked;
            oldModel.Timezone = model.Timezone;
            oldModel.Comment = model.Comment;
            await db.SaveChangesAsync();

            return this.Redirect(IndexRoute());
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var drugstore = await db.Drugstores.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (drugstore == null)
            {
                return NotFound();
            }

            var model = new DrugstoresViewModel(drugstore);

            model.Users = await db.Users.Where(x => x.DrugstoreId == id).ToListAsync();

            return View(model);
        }

        public async Task<IActionResult> CreateUser(Guid id)
        {
            var drugstore = await db.Drugstores.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            if (drugstore == null)
            {
                return NotFound();
            }

            ViewBag.Drugstore = drugstore;

            var model = new User();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(User model)
        {
            if (!ModelState.IsValid)
            {
                var drugstore = await db.Drugstores.AsNoTracking().FirstOrDefaultAsync(x => x.Id == model.DrugstoreId);
                ViewBag.Drugstore = drugstore;
                return View(model);
            }

            model.Name = model.Login;

            var result = await userManager.CreateAsync(model, model.Password);
            if (result.Succeeded)
            {
                return this.Redirect(IndexRoute());
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);
                }

                var drugstore = await db.Drugstores.AsNoTracking().FirstOrDefaultAsync(x => x.Id == model.DrugstoreId);
                ViewBag.Drugstore = drugstore;
                return View(model);
            }
        }

        public async Task<IActionResult> EditUser(Guid id)
        {
            var model = await db.Users.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            if (!model.DrugstoreId.HasValue)
            {
                throw new Exception("Из этого раздела нельзя управлять пользователями админки");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(Guid id, User model, string password)
        {
            var oldModel = await userManager.FindByIdAsync(id.ToString()); // await db.Users.AsNoTracking().FirstOrDefaultAsync(x => x.Id == model.Id);
            if (oldModel == null)
            {
                return NotFound();
            }

            if (!oldModel.DrugstoreId.HasValue)
            {
                throw new Exception("Из этого раздела нельзя управлять пользователями админки");
            }

            if (model.Id != id)
            {
                ModelState.AddModelError(nameof(model.Id), "Идентификатор не совпадает. Обратитесь к разработчикам.");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!string.IsNullOrEmpty(password))
            {
                var result = await userManager.RemovePasswordAsync(oldModel);
                if (result.Succeeded)
                {
                    var newPassResult = await userManager.AddPasswordAsync(oldModel, password);
                    if (!newPassResult.Succeeded)
                    {
                        foreach (var error in newPassResult.Errors)
                        {
                            ModelState.AddModelError(error.Code, error.Description);
                        }

                        return View(model);
                    }

                    oldModel.IsArchived = model.IsArchived;
                    await userManager.UpdateAsync(oldModel);
                }
            }
            else
            {
                oldModel.IsArchived = model.IsArchived;
                await userManager.UpdateAsync(oldModel);
            }

            return this.Redirect(IndexRoute());
        }

        public async Task<IActionResult> DeleteUser(Guid id)
        {
            var model = await userManager.FindByIdAsync(id.ToString());
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("DeleteUser")]
        public async Task<IActionResult> DeleteUserConfirm(Guid id)
        {
            var model = await userManager.FindByIdAsync(id.ToString());
            if (model == null)
            {
                return NotFound();
            }

            await userManager.DeleteAsync(model);

            return this.Redirect(IndexRoute());
        }
    }
}
