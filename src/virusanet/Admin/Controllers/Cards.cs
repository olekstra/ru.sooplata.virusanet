﻿namespace Olekstra.VirusaNet.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Model;
    using Services;
    using ViewModels;

    [Area(AreaName)]
    [Authorize(Policy = Startup.PolicyNameManager)]
    public class Cards : Controller
    {
        public const string Title = "Листовки";
        public const string AreaName = Home.AreaName;
        private const string ControllerName = nameof(Cards);

        private VirusaNetDb db;
        private IFileStorageService fileStorage;
        private ILogger logger;

        public Cards(VirusaNetDb db, IFileStorageService fileStorage, ILogger<Cards> logger)
        {
            this.db = db;
            this.fileStorage = fileStorage;
            this.logger = logger;
        }

        #region Routing

        public static UrlActionContext DetailsRoute(string id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Details),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext ListForValidationRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(ListForValidation),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext ListForValidatedRoute()
        {
            return new UrlActionContext()
            {
                Action = nameof(ListForValidated),
                Controller = ControllerName,
                Values = new { Area = AreaName }
            };
        }

        public static UrlActionContext ValidateRoute(string id)
        {
            return new UrlActionContext()
            {
                Action = nameof(Validate),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id }
            };
        }

        public static UrlActionContext ImageRoute(string id, string fileName)
        {
            return new UrlActionContext()
            {
                Action = nameof(Scan),
                Controller = ControllerName,
                Values = new { Area = AreaName, id = id, fileName = fileName }
            };
        }

        #endregion

        public async Task<IActionResult> Details(string id)
        {
            var model = new CardDetailsViewModel()
            {
                Query = id
            };

            var rx = new System.Text.RegularExpressions.Regex(@"[^\d]");

            id = rx.Replace(id, string.Empty);

            if (!Card.IsValidId(id))
            {
                model.Message = $"Недопустимый номер листовки: {id}";
                return View(model);
            }

            model.Card = await db.Cards.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model.Card == null)
            {
                model.Message = $"Листовка с указанным номером не найдена ({id})";
                return View(model);
            }

            model.CardSteps = await db.CardSteps.AsNoTracking()
                .Where(x => x.CardId == id)
                .OrderBy(x => x.StepNumber)
                .ToListAsync();

            model.Transactions = await db.Transactions.AsNoTracking()
                .Include(x => x.Drugstore)
                .Where(x => x.CardId == id)
                .OrderBy(x => x.StartTime)
                .ToListAsync();

            model.CardHistories = await db.CardHistories.AsNoTracking()
                .Include(x => x.User)
                .Where(x => x.CardId == id)
                .OrderBy(x => x.Time)
                .ToListAsync();

            return View(model);
        }

        public IActionResult Scan(string id, string fileName)
        {
            switch (fileName)
            {
                case "passport.jpg":
                case "recipe.jpg":
                    break;
                default:
                    return NotFound();
            }

            var file = fileStorage.GetCardScan(id, fileName);

            return File(file, "image/jpeg");
        }

        public async Task<IActionResult> ListForValidation()
        {
            var list = await db.Cards.AsNoTracking()
                .Where(x => x.ValidationStatus == CardValidationStatus.Unknown && !string.IsNullOrEmpty(x.Phone))
                .OrderByDescending(x => x.RegTime)
                .ToListAsync();

            return View(list);
        }

        public async Task<IActionResult> ListForValidated()
        {
            var list = await db.Cards.AsNoTracking()
                .Where(x => x.ValidationStatus != CardValidationStatus.Unknown)
                .OrderByDescending(x => x.RegTime)
                .Take(100)
                .ToListAsync();

            return View(list);
        }

        public async Task<IActionResult> Validate(string id)
        {
            var model = await db.Cards.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            if (model.ValidationStatus != CardValidationStatus.Unknown && !string.IsNullOrEmpty(model.Phone))
            {
                throw new Exception("Wrond Status: " + model.ValidationStatus);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Validate(
            string id,
            CardValidationStatus validationStatus,
            string comment,
            [FromServices] ICardStepMoverService cardStepMoverService,
            [FromServices] UserManager<User> userManager)
        {
            var model = await db.Cards.SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            if (model.ValidationStatus != CardValidationStatus.Unknown && !string.IsNullOrEmpty(model.Phone))
            {
                throw new Exception("Wrond Status: " + model.ValidationStatus);
            }

            model.ValidationStatus = validationStatus;
            model.ValidationTime = DateTimeOffset.Now;

            var userId = Guid.Parse(userManager.GetUserId(User));

            db.CardHistories.Add(new CardHistory
            {
                Id = Guid.NewGuid(),
                CardId = id,
                Time = DateTimeOffset.Now,
                UserId = userId,
                Comment = "Статус: " + validationStatus.GetDescription() + "; " + Environment.NewLine + comment
            });

            await db.SaveChangesAsync();

            await cardStepMoverService.CheckCardAsync(id, userId);

            return this.Redirect(ListForValidationRoute());
        }
    }
}
