﻿namespace Microsoft.AspNetCore.Mvc
{
    using Routing;

    public static class ControllerExtensions
    {
        public static IActionResult Redirect(this Controller controller, UrlActionContext value)
        {
            var url = controller.Url.Action(value);
            return controller.Redirect(url);
        }
    }
}
