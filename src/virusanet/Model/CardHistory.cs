﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CardHistory
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string CardId { get; set; }

        [ForeignKey("CardId")]
        public virtual Card Card { get; set; }

        public DateTimeOffset Time { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }
    }
}
