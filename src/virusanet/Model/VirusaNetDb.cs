﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;

    using Npgsql.EntityFrameworkCore.PostgreSQL;

    public partial class VirusaNetDb : DbContext
    {
        public VirusaNetDb(DbContextOptions<VirusaNetDb> options)
            : base(options)
        {
            // Nothing
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Drugstore> Drugstores { get; set; }

        public DbSet<Card> Cards { get; set; }

        public DbSet<CardHistory> CardHistories { get; set; }

        public DbSet<CardRegistration> CardRegistrations { get; set; }

        public DbSet<CardStep> CardSteps { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<TransactionHistory> TransactionHistories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.Entity<Drugstore>(t =>
            {
                t.HasIndex(e => e.Name);
            });

            modelBuilder.Entity<Card>(t =>
            {
                t.HasIndex(e => e.Phone);
                t.HasIndex(e => e.ValidationStatus);
            });

            modelBuilder.Entity<CardStep>(t =>
            {
                t.HasOne(p => p.Transaction).WithOne().OnDelete(DeleteBehavior.Restrict);
                t.HasIndex(e => new { e.CardId, e.StepNumber }).IsUnique();
                t.HasIndex(e => e.IsOpenNotificationSent);
            });

            modelBuilder.Entity<CardRegistration>(t =>
            {
                t.HasIndex(e => e.Expires);
            });

            modelBuilder.Entity<Transaction>(t =>
            {
                t.HasIndex(e => e.Status);
                t.HasIndex(e => e.CardId);
            });
        }
    }
}
