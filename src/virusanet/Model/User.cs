﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        public static readonly Guid BackgroundUserId = new Guid("4E01178F-E5DD-430D-B07C-D948AE6FD6D3");

        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        [MaxLength(100)]
        public string NormalizedEmail { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string NormalizedName { get; set; }

        [MaxLength(100)]
        public string Login { get; set; }

        [MaxLength(100)]
        public string Password { get; set; }

        public Guid? DrugstoreId { get; set; }

        public Guid SecurityStamp { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsArchived { get; set; }
    }
}
