﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;

    public class Card
    {
        public static readonly Expression<Func<Card, bool>> ActiveCardExpression =
            (card) => !card.IsLocked && !card.IsFullyUtilized && card.ValidationStatus == CardValidationStatus.Validated;

        // компиляция не кешируется, так что компилируем сразу
        public static readonly Func<Card, bool> ActiveCardFunc = ActiveCardExpression.Compile();

        private static readonly Regex IdPattern = new Regex(@"^ \d{13} $", RegexOptions.IgnorePatternWhitespace);

        [Key]
        [MaxLength(20)]
        public string Id { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        public DateTimeOffset RegTime { get; set; }

        public CardValidationStatus ValidationStatus { get; set; }

        public DateTimeOffset ValidationTime { get; set; }

        /// <summary>
        /// Карта заблокирована и операции по ней запрещены
        /// </summary>
        public bool IsLocked { get; set; }

        /// <summary>
        /// Карта полностью использована, и по ней можно уже "ничего не ловить"
        /// </summary>
        public bool IsFullyUtilized { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }

        public static bool IsValidId(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            return IdPattern.IsMatch(value);
        }

        public string GetFormattedId()
        {
            return Id.Substring(0, 3) + " " + Id.Substring(3, 3) + " " + Id.Substring(6, 3) + " " + Id.Substring(9);
        }

        public string GetStatusText()
        {
            if (IsLocked)
            {
                return "Заблокирована (не действует)";
            }

            if (string.IsNullOrEmpty(Phone))
            {
                return "Новая (не активирована)";
            }

            if (ValidationStatus == CardValidationStatus.Unknown)
            {
                return "Ожидает проверки держателя";
            }

            if (ValidationStatus == CardValidationStatus.Rejected)
            {
                return "Проверка держателя выполнена отрицательно";
            }

            if (IsFullyUtilized)
            {
                return "Полностью использована";
            }

            return "Действует (активна)";
        }
    }
}
