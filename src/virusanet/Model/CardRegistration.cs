﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;

    public class CardRegistration
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string CardId { get; set; }

        [ForeignKey("CardId")]
        public virtual Card Card { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(10)]
        public string ConfirmationCode { get; set; }

        public DateTimeOffset Expires { get; set; }
    }
}
