﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.ComponentModel;

    public enum CardValidationStatus : byte
    {
        [Description("Новая")]
        Unknown,

        [Description("Проверена")]
        Validated,

        [Description("Не прошла проверку")]
        Rejected
    }
}
