﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Шаги по программе, достигнутые картой
    /// </summary>
    public class CardStep
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string CardId { get; set; }

        [ForeignKey("CardId")]
        public virtual Card Card { get; set; }

        [Required]
        public byte StepNumber { get; set; }

        /// <summary>
        /// Дата когда данный шаг станет доступен
        /// </summary>
        public DateTimeOffset OpenTime { get; set; }

        public bool IsOpenNotificationSent { get; set; }

        public Guid? TransactionId { get; set; }

        [ForeignKey("TransactionId")]
        public virtual Transaction Transaction { get; set; }
    }
}
