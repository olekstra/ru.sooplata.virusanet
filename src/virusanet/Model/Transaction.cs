﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Transaction
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid DrugstoreId { get; set; }

        [ForeignKey("DrugstoreId")]
        public virtual Drugstore Drugstore { get; set; }

        [Required]
        [MaxLength(20)]
        public string CardId { get; set; }

        [ForeignKey("CardId")]
        public virtual Card Card { get; set; }

        [Required]
        public byte StepNumber { get; set; }

        public DateTimeOffset StartTime { get; set; }

        [Required]
        [MaxLength(10)]
        public string ConfirmationCode { get; set; }

        public TransactionStatus Status { get; set; }

        [MaxLength(100)]
        public string ScanFileName { get; set; }
    }
}
