﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Drugstore
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(200)]
        public string Timezone { get; set; }

        /// <summary>
        /// Аптека заблокирована и не может выполнять операции и вообще работать в системе
        /// </summary>
        public bool IsLocked { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }
    }
}
