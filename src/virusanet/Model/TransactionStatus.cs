﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.ComponentModel;

    public enum TransactionStatus : byte
    {
        [Description("Начата")]
        Started,

        [Description("Контрольный код введён")]
        Confirmed,

        [Description("Завершена")]
        Completed,

        [Description("Отменена")]
        Aborted,

        [Description("Ожидает проверки")]
        AwaitingValidation,

        [Description("Проверена (службой контроля)")]
        Validated,

        [Description("Отвергнута (службой контроля)")]
        Rejected
    }
}
