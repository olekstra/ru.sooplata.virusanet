﻿namespace Olekstra.VirusaNet
{
    using System;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Копия из Olekstra.CommonNext (не тащить же весь пакет ради одного класса)
    /// </summary>
    public struct PhoneNumber
    {
        private static readonly Regex OlekstraPattern = new Regex(
            @"^ (\d{3})(\d{3})(\d{2})(\d{2}) $",
            RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

        private static readonly Regex UserPattern = new Regex(
            @"^\s* (?:\+7|8) [\s\-\(]* (\d{3}) [\)\s\-]* (\d{3}) [\s\-]* (\d{2}) [\-\s]* (\d{2}) \s* $",
            RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

        private string number;

        public PhoneNumber(string number)
            : this(number, false)
        {
            // Nothing
        }

        private PhoneNumber(string number, bool skipValidation)
        {
            if (skipValidation)
            {
                this.number = number;
            }
            else
            {
                this.number = ToCanonicalNumber(number);
                if (this.number == null)
                {
                    throw new ArgumentException(nameof(number));
                }
            }
        }

        public static PhoneNumber Empty { get; } = new PhoneNumber("0000000000", true);

        #region Equality overloading, from http://stackoverflow.com/questions/1502451/

        public static bool operator ==(PhoneNumber a, PhoneNumber b)
        {
            return a.number == b.number;
        }

        public static bool operator !=(PhoneNumber a, PhoneNumber b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            return obj is PhoneNumber && this == (PhoneNumber)obj;
        }

        public bool Equals(PhoneNumber p)
        {
            return p.number == this.number;
        }

        public override int GetHashCode()
        {
            return number.GetHashCode();
        }

        #endregion

        public static bool TryParse(string value, out PhoneNumber result)
        {
            var canonical = ToCanonicalNumber(value);

            if (canonical == null)
            {
                result = PhoneNumber.Empty;
                return false;
            }

            result = new PhoneNumber(canonical, true);
            return true;
        }

        public string ToOlekstraPhone()
        {
            return number;
        }

        public string ToBeautyPhone()
        {
            var group0 = number.Substring(0, 3);
            var group1 = number.Substring(3, 3);
            var group2 = number.Substring(6, 2);
            var group3 = number.Substring(8, 2);

            return $"+7 {group0} {group1}-{group2}-{group3}";
        }

        public override string ToString()
        {
            return number;
        }

        private static string ToCanonicalNumber(string value)
        {
            if (value == null)
            {
                return null;
            }

            var match = OlekstraPattern.Match(value);
            if (!match.Success)
            {
                match = UserPattern.Match(value);
                if (!match.Success)
                {
                    return null;
                }
            }

            var group0 = match.Groups[1].Value;
            var group1 = match.Groups[2].Value;
            var group2 = match.Groups[3].Value;
            var group3 = match.Groups[4].Value;

            return group0 + group1 + group2 + group3;
        }
    }
}
