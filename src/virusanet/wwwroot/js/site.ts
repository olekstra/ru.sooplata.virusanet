﻿/// <reference path="jquery.d.ts" />

function prepareFormsCommon(data: any) {
    $('.token').attr('name', data.name).val(data.value);

    $.ajaxSetup({
        error: function () {
            $('#server-error')
                .text("Произошла непредвиденная ошибка на сервере. Повторите операцию ещё раз, если ошибка сохранится - обратитесь в колл-центр.")
                .show();
            $('#qualForm-submit').removeProp('disabled').removeClass('btn-u-default');
            $('#regForm-submit').removeProp('disabled').removeClass('btn-u-default');
        }
    });
}

/// INDEX-QUAL

var qualFormTokenOk = false;
var qualPatterns: string[] = null;
var qualFormTimer = null;

function initQual() {
    $.getJSON('/token', prepareQualFormToken);
    $.getJSON('/qualPatterns', prepareQualFormPatterns);
}

function prepareQualFormToken(data: any) {
    prepareFormsCommon(data);
    qualFormTokenOk = true;
    console.log("prepareQualFormToken() done");
    if (qualPatterns != null) {
        prepareQualForm();
    }
}

function prepareQualFormPatterns(data: string[]) {
    qualPatterns = data;
    console.log("qualPatterns() done", data);
    if (qualFormTokenOk) {
        prepareQualForm();
    }
}

function prepareQualForm() {
    $('#qualForm-text').keyup(function (e) {
        if (e.keyCode != 13) {
            if (qualFormTimer != null) {
                clearTimeout(qualFormTimer);
            }
            qualFormTimer = setTimeout(validateQualForm, 300);
        }
    });
    $('#qualForm').submit(handleQualSubmit);
    console.log("prepareQualForm() complete.");
}

function validateQualForm() {
    var text = $('#qualForm-text').val().toUpperCase().trim() as string;
    var isOk = false;

    for (var i in qualPatterns) {
        var pattern = qualPatterns[i];
        if (pattern.slice(-1) == "*") {
            pattern = pattern.slice(0, -1).toUpperCase();
            if (text.indexOf(pattern) == 0) {
                console.log("validateQualForm: like pattern: " + pattern);
                isOk = true;
                break;
            }
        }
        else {
            if (text.toUpperCase() == pattern) {
                console.log("validateQualForm: equal to pattern: " + pattern);
                isOk = true;
                break;
            }
        }
    }

    if (isOk) {
        $('#qualForm-submit').removeProp('disabled').removeClass('btn-u-default');
    }
    else {
        $('#qualForm-submit').prop('disabled', 'disabled').addClass('btn-u-default');
    }

    return isOk;
}

function handleQualSubmit(e) {
    var isOk = validateQualForm();
    if (!isOk) {
        e.preventDefault();
    }
}

/// REG

var step = 1;

interface ServerResponse {
    status: boolean;
    message: string;
}

function initReg() {
    $('#regForm').submit(submitRegForm)

    $.getJSON('/token', prepareRegForm);
}

function prepareRegForm(data: any) {
    prepareFormsCommon(data);

    $('#server-error').hide().removeClass('hidden');
    $('#phonecode-group').hide().removeClass('hidden');
    $('#phonecode').prop('disabled', 'disabled');

    console.log("prepareRegForm done.");
}

function submitRegForm(evt) {
    console.log("in submitRegForm()");
    evt.preventDefault();
    $('#server-error').hide();

    if (step == 1) {
        submitRegForm1();
        return;
    }
    if (step == 2) {
        submitRegForm2();
        return;
    }
    console.log("submitRegForm(): unknown step:", step);
}

function submitRegForm1() {
    $('#server-error').hide();
    $.post('/reg1', $('#regForm').serializeArray(), successRegForm1);
    $('#regForm-submit').prop('disabled', 'disabled').addClass('btn-u-default');
    console.debug("submitRegForm1() done");
}

function successRegForm1(data: ServerResponse) {
    console.log("step 1: in successForm1(): ", data);
    $('#regForm-submit').removeProp('disabled').removeClass('btn-u-default');
    if (data.status != true) {
        $('#server-error').text(data.message).show();
        return;
    }

    step = 2;
    $('#phonecode').removeProp('disabled');
    $('#phonecode-group').show();

    if (data.message) {
        alert(data.message);
    }

    console.log("successRegForm1() done ");
}

function submitRegForm2() {
    $('#server-error').hide();
    let data = new FormData(<HTMLFormElement>$('#regForm')[0]);
    $('#regForm-submit').prop('disabled', 'disabled').addClass('btn-u-default');
    $.ajax(
        "/reg2",
        {
            data: data,
            method: 'POST',
            processData: false,
            contentType: false,
            error: function () {
                let text = 'Произошла ошибка на сервере. Пожалуйста, попробуйте ещё раз, если ошибка сохранится - позвоните по телефону горячей линии.';
                $('#server-error').text(text).show();
                $('#regForm-submit').removeProp('disabled').removeClass('btn-u-default');
            },
            success: function (data: ServerResponse, textStatus) {
                if (data.status) {
                    (<HTMLFormElement>$('#regForm')[0]).reset();
                    $('#regForm-submit').hide();
                    alert('Регистрация успешно завершена');
                } else {
                    $('#server-error').text(data.message).show();
                    $('#regForm-submit').removeProp('disabled').removeClass('btn-u-default');
                }
            }
        }
    );
}
