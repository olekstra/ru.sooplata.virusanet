﻿namespace Olekstra.VirusaNet.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Drugstore_Offset_to_Timezone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Offset",
                table: "Drugstores");

            migrationBuilder.AddColumn<string>(
                name: "Timezone",
                table: "Drugstores",
                maxLength: 200,
                nullable: false,
                defaultValue: string.Empty);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Timezone",
                table: "Drugstores");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "Offset",
                table: "Drugstores",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }
    }
}
