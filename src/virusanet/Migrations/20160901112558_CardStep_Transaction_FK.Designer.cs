﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Olekstra.VirusaNet.Model;

namespace Olekstra.VirusaNet.Migrations
{
    [DbContext(typeof(VirusaNetDb))]
    [Migration("20160901112558_CardStep_Transaction_FK")]
    partial class CardStep_Transaction_FK
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Olekstra.VirusaNet.Model.Card", b =>
                {
                    b.Property<string>("Id")
                        .HasAnnotation("MaxLength", 20);

                    b.Property<string>("Comment")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<bool>("IsFullyUtilized");

                    b.Property<bool>("IsLocked");

                    b.Property<string>("Phone")
                        .HasAnnotation("MaxLength", 20);

                    b.Property<DateTimeOffset>("RegTime");

                    b.Property<byte>("ValidationStatus");

                    b.Property<DateTimeOffset>("ValidationTime");

                    b.HasKey("Id");

                    b.HasIndex("Phone");

                    b.HasIndex("ValidationStatus");

                    b.ToTable("Cards");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.CardHistory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CardId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 20);

                    b.Property<string>("Comment")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<DateTimeOffset>("Time");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("CardId");

                    b.HasIndex("UserId");

                    b.ToTable("CardHistories");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.CardStep", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CardId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 20);

                    b.Property<bool>("IsOpenNotificationSent");

                    b.Property<DateTimeOffset>("OpenTime");

                    b.Property<byte>("StepNumber");

                    b.Property<Guid?>("TransactionId");

                    b.HasKey("Id");

                    b.HasIndex("CardId");

                    b.HasIndex("IsOpenNotificationSent");

                    b.HasIndex("TransactionId")
                        .IsUnique();

                    b.HasIndex("CardId", "StepNumber")
                        .IsUnique();

                    b.ToTable("CardSteps");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.Drugstore", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<bool>("IsLocked");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Timezone")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.HasKey("Id");

                    b.HasIndex("Name");

                    b.ToTable("Drugstores");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.Transaction", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CardId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 20);

                    b.Property<string>("ConfirmationCode")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 10);

                    b.Property<Guid>("DrugstoreId");

                    b.Property<DateTimeOffset>("StartTime");

                    b.Property<byte>("Status");

                    b.Property<byte>("StepNumber");

                    b.HasKey("Id");

                    b.HasIndex("CardId");

                    b.HasIndex("DrugstoreId");

                    b.HasIndex("Status");

                    b.ToTable("Transactions");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.TransactionHistory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<DateTimeOffset>("Time");

                    b.Property<Guid>("TransactionId");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("TransactionId");

                    b.HasIndex("UserId");

                    b.ToTable("TransactionHistories");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("DrugstoreId");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<bool>("IsAdmin");

                    b.Property<bool>("IsArchived");

                    b.Property<string>("Login")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("NormalizedEmail")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("Password")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<Guid>("SecurityStamp");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.CardHistory", b =>
                {
                    b.HasOne("Olekstra.VirusaNet.Model.Card", "Card")
                        .WithMany()
                        .HasForeignKey("CardId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Olekstra.VirusaNet.Model.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.CardStep", b =>
                {
                    b.HasOne("Olekstra.VirusaNet.Model.Card", "Card")
                        .WithMany()
                        .HasForeignKey("CardId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Olekstra.VirusaNet.Model.Transaction", "Transaction")
                        .WithOne()
                        .HasForeignKey("Olekstra.VirusaNet.Model.CardStep", "TransactionId");
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.Transaction", b =>
                {
                    b.HasOne("Olekstra.VirusaNet.Model.Card", "Card")
                        .WithMany()
                        .HasForeignKey("CardId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Olekstra.VirusaNet.Model.Drugstore", "Drugstore")
                        .WithMany()
                        .HasForeignKey("DrugstoreId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Olekstra.VirusaNet.Model.TransactionHistory", b =>
                {
                    b.HasOne("Olekstra.VirusaNet.Model.Transaction", "Transaction")
                        .WithMany()
                        .HasForeignKey("TransactionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Olekstra.VirusaNet.Model.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
