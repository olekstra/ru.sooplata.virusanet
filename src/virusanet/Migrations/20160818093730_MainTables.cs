﻿namespace Olekstra.VirusaNet.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class MainTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cards",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 20, nullable: false),
                    Comment = table.Column<string>(maxLength: 1000, nullable: true),
                    IsLocked = table.Column<bool>(nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    RegTime = table.Column<DateTimeOffset>(nullable: false),
                    ValidationStatus = table.Column<byte>(nullable: false),
                    ValidationTime = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cards", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Drugstores",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Comment = table.Column<string>(maxLength: 1000, nullable: true),
                    IsLocked = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Offset = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drugstores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CardSteps",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CardId = table.Column<string>(maxLength: 20, nullable: false),
                    IsOpenNotificationSent = table.Column<bool>(nullable: false),
                    OpenTime = table.Column<DateTimeOffset>(nullable: false),
                    StepNumber = table.Column<byte>(nullable: false),
                    TransactionId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardSteps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CardSteps_Cards_CardId",
                        column: x => x.CardId,
                        principalTable: "Cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CardId = table.Column<string>(maxLength: 20, nullable: false),
                    ConfirmationCode = table.Column<string>(maxLength: 10, nullable: false),
                    DrugstoreId = table.Column<Guid>(nullable: false),
                    StartTime = table.Column<DateTimeOffset>(nullable: false),
                    Status = table.Column<byte>(nullable: false),
                    StepNumber = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_Cards_CardId",
                        column: x => x.CardId,
                        principalTable: "Cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transactions_Drugstores_DrugstoreId",
                        column: x => x.DrugstoreId,
                        principalTable: "Drugstores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransactionHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Comment = table.Column<string>(maxLength: 1000, nullable: true),
                    Time = table.Column<DateTimeOffset>(nullable: false),
                    TransactionId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionHistories_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransactionHistories_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cards_Phone",
                table: "Cards",
                column: "Phone");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_ValidationStatus",
                table: "Cards",
                column: "ValidationStatus");

            migrationBuilder.CreateIndex(
                name: "IX_CardSteps_CardId",
                table: "CardSteps",
                column: "CardId");

            migrationBuilder.CreateIndex(
                name: "IX_CardSteps_IsOpenNotificationSent",
                table: "CardSteps",
                column: "IsOpenNotificationSent");

            migrationBuilder.CreateIndex(
                name: "IX_CardSteps_CardId_StepNumber",
                table: "CardSteps",
                columns: new[] { "CardId", "StepNumber" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Drugstores_Name",
                table: "Drugstores",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CardId",
                table: "Transactions",
                column: "CardId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_DrugstoreId",
                table: "Transactions",
                column: "DrugstoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_Status",
                table: "Transactions",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistories_TransactionId",
                table: "TransactionHistories",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionHistories_UserId",
                table: "TransactionHistories",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CardSteps");

            migrationBuilder.DropTable(
                name: "TransactionHistories");

            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Cards");

            migrationBuilder.DropTable(
                name: "Drugstores");
        }
    }
}
