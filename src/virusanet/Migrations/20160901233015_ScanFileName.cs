﻿namespace Olekstra.VirusaNet.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class ScanFileName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ScanFileName",
                table: "Transactions",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScanFileName",
                table: "Transactions");
        }
    }
}
