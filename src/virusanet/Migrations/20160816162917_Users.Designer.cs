﻿namespace Olekstra.VirusaNet.Migrations
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Migrations;
    using Olekstra.VirusaNet.Model;

    [DbContext(typeof(VirusaNetDb))]
    [Migration("20160816162917_Users")]
    partial class Users
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Olekstra.VirusaNet.Model.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("DrugstoreId");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<bool>("IsAdmin");

                    b.Property<bool>("IsArchived");

                    b.Property<string>("Login")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("NormalizedEmail")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("Password")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<Guid>("SecurityStamp");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });
        }
    }
}
