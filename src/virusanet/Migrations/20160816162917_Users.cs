﻿namespace Olekstra.VirusaNet.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            if (migrationBuilder.ActiveProvider == nameof(Npgsql.EntityFrameworkCore.PostgreSQL))
            {
                migrationBuilder.CreatePostgresExtension("uuid-ossp");
            }

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DrugstoreId = table.Column<Guid>(nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false),
                    IsArchived = table.Column<bool>(nullable: false),
                    Login = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    NormalizedEmail = table.Column<string>(maxLength: 100, nullable: false),
                    NormalizedName = table.Column<string>(maxLength: 200, nullable: true),
                    Password = table.Column<string>(maxLength: 100, nullable: true),
                    SecurityStamp = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            if (migrationBuilder.ActiveProvider == nameof(Npgsql.EntityFrameworkCore.PostgreSQL))
            {
                migrationBuilder.DropPostgresExtension("uuid-ossp");
            }

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
