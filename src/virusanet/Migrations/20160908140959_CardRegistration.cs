﻿namespace Olekstra.VirusaNet.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class CardRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CardRegistrations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CardId = table.Column<string>(maxLength: 20, nullable: false),
                    ConfirmationCode = table.Column<string>(maxLength: 10, nullable: true),
                    Expires = table.Column<DateTimeOffset>(nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CardRegistrations_Cards_CardId",
                        column: x => x.CardId,
                        principalTable: "Cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CardRegistrations_CardId",
                table: "CardRegistrations",
                column: "CardId");

            migrationBuilder.CreateIndex(
                name: "IX_CardRegistrations_Expires",
                table: "CardRegistrations",
                column: "Expires");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CardRegistrations");
        }
    }
}
