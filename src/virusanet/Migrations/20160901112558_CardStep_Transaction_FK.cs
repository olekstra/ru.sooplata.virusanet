﻿namespace Olekstra.VirusaNet.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class CardStep_Transaction_FK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_CardSteps_TransactionId",
                table: "CardSteps",
                column: "TransactionId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CardSteps_Transactions_TransactionId",
                table: "CardSteps",
                column: "TransactionId",
                principalTable: "Transactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CardSteps_Transactions_TransactionId",
                table: "CardSteps");

            migrationBuilder.DropIndex(
                name: "IX_CardSteps_TransactionId",
                table: "CardSteps");
        }
    }
}
