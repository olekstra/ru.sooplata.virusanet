﻿namespace Olekstra.VirusaNet
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.Unicode;
    using System.Threading.Tasks;

    using Logging.ExceptionSender;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Razor;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    using Model;
    using RecurrentTasks;
    using Services;

    public class Startup
    {
        public const string PolicyNameAdmin = "Admin";

        public const string PolicyNameManager = "Manager";

        public const string PolicyNameDrugstore = "Drugstore";

        private bool isDevelopment;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json");

            // Workaround for https://github.com/aspnet/UserSecrets/issues/62
            if (builder.GetFileProvider().GetFileInfo("project.json").Exists)
            {
                builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();

            Configuration = builder.Build();

            isDevelopment = env.IsDevelopment();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationFormats.Clear();
                options.ViewLocationFormats.Add("/Public/Views/{1}/{0}.cshtml");
                options.ViewLocationFormats.Add("/Public/Views/Shared/{0}.cshtml");
                options.AreaViewLocationFormats.Clear();
                options.AreaViewLocationFormats.Add("/{2}/Views/{1}/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/{2}/Views/Shared/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Public/Views/Shared/{0}.cshtml");
            });

            var dbProvider = Configuration["DB:Provider"];
            var dbConnString = string.Empty;
            switch (dbProvider)
            {
                case "SqlServer":
                    dbConnString = Configuration["DB:ConnectionStringMSSQL"];
                    services.AddDbContext<VirusaNetDb>(o => o.UseSqlServer(dbConnString));
                    break;
                case "PostgreSql":
                    dbConnString = Configuration["DB:ConnectionStringPGSQL"];
                    services.AddDbContext<VirusaNetDb>(o => o.UseNpgsql(dbConnString));
                    break;
                default:
                    throw new Exception($"Unknown DB Provider: '{dbProvider}'");
            }

            services.AddIdentity<User, UserRole>()
                .AddUserStore<VirusaNetDb>()
                .AddRoleStore<VirusaNetDb>();

            services.Configure<IdentityOptions>(options =>
            {
                options.User.AllowedUserNameCharacters = null;
            });

            services.Configure<RouteOptions>(o =>
            {
                o.LowercaseUrls = true;
            });

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });

            services.Configure<AuthorizationOptions>(options =>
            {
                options.AddPolicy(PolicyNameAdmin, policy => policy.RequireClaim("IsAdmin", "true"));
                options.AddPolicy(PolicyNameManager, policy => policy.RequireClaim("IsManager", "true"));
                options.AddPolicy(PolicyNameDrugstore, policy => policy.RequireClaim("DrugstoreId"));
            });

            services.AddTransient(typeof(BootstrapMvc.Mvc6.BootstrapHelper<>));

            services.AddScoped<Apteka.IRetailService, Apteka.RetailService>();
            services.Configure<BusinessRulesOptions>(Configuration.GetSection("BusinessRules"));
            services.Configure<FileStorageOptions>(Configuration.GetSection("FileStorage"));
            services.AddScoped<IFileStorageService, FileStorageService>();
            services.AddScoped<ICardStepMoverService, CardStepMoverService>();
            services.Configure<SlackMessageServiceOptions>(Configuration.GetSection("SlackMessageService"));
            services.Configure<SmscruMessageServiceOptions>(Configuration.GetSection("SmscruMessageService"));
            if (isDevelopment)
            {
                services.AddScoped<IMessageService, SlackMessageService>();
            }
            else
            {
                services.AddScoped<IMessageService, SmscruMessageService>();
            }

            services.AddSingleton<QualificationService>();
            services.AddSingleton<CardRegistrationService>();

            services.AddSingleton<ExceptionSenderTask, ExceptionSenderMailgunTask>();
            services.Configure<ExceptionSenderOptions>(Configuration.GetSection("ExceptionSender"));
            services.Configure<ExceptionSenderMailgunOptions>(Configuration.GetSection("ExceptionSender"));
            services.AddSingleton<CardStepNotificationsSenderTask>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddMemory(Configuration.GetSection("Logging"));

            app.UseDeveloperExceptionPage();

            app.UseStatusCodePages();

            app.UseExceptionSender();

            if (env.IsDevelopment())
            {
                app.UseStaticFiles();
            }
            else
            {
                var staticFileOptions = new StaticFileOptions
                {
                    OnPrepareResponse = (context) =>
                    {
                        context.Context.Response.Headers.Add("Cache-Control", "public, max-age=15552000"); // 180 days
                    }
                };
                app.UseStaticFiles();
            }

            app.UseIdentity();
            app.UseGoogleAuthentication(new GoogleOptions
            {
                ClientId = Configuration["OAuth:Google:ClientId"],
                ClientSecret = Configuration["OAuth:Google:ClientSecret"]
            });

            var ruCulture = new CultureInfo("ru-RU");
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                SupportedCultures = new[] { ruCulture }
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });

            CreateSampleData(app, Configuration["SystemAdminEmail"]);

            var exceptionSenderTask = app.ApplicationServices.GetRequiredService<ExceptionSenderTask>();
            if (!env.IsDevelopment())
            {
                exceptionSenderTask.Start();
            }

            app.ApplicationServices.GetRequiredService<CardStepNotificationsSenderTask>()
                .CatchExceptions(exceptionSenderTask).Start();
        }

        protected void CreateSampleData(IApplicationBuilder app, string adminEmail)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var db = serviceScope.ServiceProvider.GetService<VirusaNetDb>();

                db.Database.Migrate();

                // step 1: recreate admin
                var admin = db.Users.FirstOrDefaultAsync(x => x.Email == adminEmail).Result;
                if (admin == null)
                {
                    admin = new User
                    {
                        Id = Guid.NewGuid(),
                        Name = adminEmail,
                        NormalizedName = adminEmail.ToUpperInvariant(),
                        Email = adminEmail,
                        NormalizedEmail = adminEmail.ToUpperInvariant(),
                        IsAdmin = true
                    };
                    db.Users.Add(admin);
                    db.SaveChanges();
                }
                else
                {
                    if (!admin.IsAdmin || admin.IsArchived)
                    {
                        admin.IsAdmin = true;
                        admin.IsArchived = false;
                        db.SaveChanges();
                    }
                }

                // step 2: recreate background user
                var bgUser = db.Users.FirstOrDefaultAsync(x => x.Id == User.BackgroundUserId).Result;
                if (bgUser == null)
                {
                    bgUser = new User
                    {
                        Id = User.BackgroundUserId,
                        Name = "Automatic",
                        NormalizedName = "Automatic".ToUpperInvariant(),
                        Email = "automatic",
                        NormalizedEmail = "automatic".ToUpperInvariant(),
                        IsAdmin = false
                    };
                    db.Users.Add(bgUser);
                    db.SaveChanges();
                }
            }
        }
    }
}
