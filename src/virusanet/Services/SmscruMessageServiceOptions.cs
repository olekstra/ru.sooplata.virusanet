﻿namespace Olekstra.VirusaNet.Services
{
    public class SmscruMessageServiceOptions
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public string Sender { get; set; }
    }
}
