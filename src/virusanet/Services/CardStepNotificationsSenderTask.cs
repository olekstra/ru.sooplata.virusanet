﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Olekstra.VirusaNet.Model;
    using RecurrentTasks;

    public class CardStepNotificationsSenderTask : TaskBase<TaskRunStatus>
    {
        public CardStepNotificationsSenderTask(ILoggerFactory loggerFactory, IServiceScopeFactory serviceScopeFactory)
            : base(loggerFactory, TimeSpan.FromMinutes(5), serviceScopeFactory)
        {
            // nothing
        }

        protected override void Run(IServiceProvider serviceProvider, TaskRunStatus runStatus)
        {
            var db = serviceProvider.GetRequiredService<VirusaNetDb>();

            var now = DateTimeOffset.Now;

            var notifications = db.CardSteps
                .Include(x => x.Card)
                .Where(x => !x.IsOpenNotificationSent && x.OpenTime < now)
                .OrderBy(x => x.OpenTime)
                .Take(10)
                .ToList();

            if (notifications.Count == 0)
            {
                Logger.LogDebug("Нет неотправленных уведомлений");
                return;
            }

            var sender = serviceProvider.GetRequiredService<IMessageService>();
            var options = serviceProvider.GetRequiredService<IOptions<BusinessRulesOptions>>().Value;

            foreach (var item in notifications)
            {
                var stepInfo = options.AllowedCardSteps[item.StepNumber - 1]; // потому что в БД с единицы

                // добавляем пробелы чтобы отличались тексты 2 и 3 шагов
                var mix2 = item.StepNumber == 2 ? " " : string.Empty;
                var mix3 = item.StepNumber == 3 ? " " : string.Empty;

                var text = $"Доступна {mix2}возможность {mix3}приобрести \"{stepInfo.Name}\" по цене {stepInfo.Price}";

                sender.SendAsync(new PhoneNumber(item.Card.Phone), text).Wait();

                item.IsOpenNotificationSent = true;
                db.SaveChanges();

                Logger.LogInformation($"Отправлено на {item.Card.Phone}: {text}");
            }
        }
    }
}
