﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// Для тестов - отправляем в Slack :)
    /// </summary>
    public class SlackMessageService : IMessageService
    {
        private SlackMessageServiceOptions options;

        public SlackMessageService(IOptions<SlackMessageServiceOptions> options)
        {
            this.options = options.Value;
        }

        public async Task SendAsync(PhoneNumber phone, string message)
        {
            var data = new { text = message };
            var jsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            using (var httpClient = new HttpClient())
            {
                var content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(new Uri(options.WebhookUrl), content);
                response.EnsureSuccessStatusCode();
            }
        }
    }
}
