﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Olekstra.VirusaNet.Model;
    using Public.ViewModels;

    public class CardRegistrationService
    {
        private static readonly Random ConfirmationCodeGenerator = new Random();

        private VirusaNetDb db;
        private IMessageService messageService;
        private ILogger logger;

        public CardRegistrationService(VirusaNetDb db, IMessageService messageService, ILogger<CardRegistrationService> logger)
        {
            this.db = db;
            this.messageService = messageService;
            this.logger = logger;
        }

        public async Task<ResponseResult> RegStep1Async(string cardNumber, PhoneNumber phone)
        {
            if (!Card.IsValidId(cardNumber))
            {
                throw new ArgumentNullException(nameof(cardNumber));
            }

            if (PhoneNumber.Empty == phone)
            {
                throw new ArgumentNullException(nameof(phone));
            }

            var card = await db.Cards.SingleOrDefaultAsync(x => x.Id == cardNumber);
            if (card == null)
            {
                return ResponseResult.Fail("Листовка с указанным номером не обнаружена. Проверьте правильность ввода.");
            }

            if (card.Phone == phone.ToString())
            {
                return ResponseResult.Fail("Листовка уже зарегистрирована на ваш номер телефона. Повторная регистрация не требуется.");
            }

            var existingActiveCards = await db.Cards
                .Where(x => x.Phone == phone.ToString())
                .Where(Card.ActiveCardExpression)
                .CountAsync();
            if (existingActiveCards > 0)
            {
                return ResponseResult.Fail("На ваш номер телефона уже зарегистрирована действующая листовка. Используйте первую листовку полностью, и только потом можно будет активировать ещё одну.");
            }

            if (!string.IsNullOrEmpty(card.Phone))
            {
                return ResponseResult.Fail("Листовка уже зарегистрирована на другой номер телефона. Проверьте правильность ввода номера.");
            }

            if (card.IsLocked)
            {
                return ResponseResult.Fail("Листовка не может быть зарегистрирована. Позвоните на горячую линию для уточнения деталей.");
            }

            var cardRegistration = await db.CardRegistrations
                .Where(x => x.CardId == card.Id)
                .Where(x => x.Phone == phone.ToString())
                .Where(x => x.Expires > DateTimeOffset.Now)
                .FirstOrDefaultAsync();
            if (cardRegistration == null)
            {
                cardRegistration = new CardRegistration
                {
                    Id = Guid.NewGuid(),
                    CardId = card.Id,
                    Phone = phone.ToString(),
                    ConfirmationCode = ConfirmationCodeGenerator.Next(10000, 99999).ToString()
                };
                db.CardRegistrations.Add(cardRegistration);
                logger.LogDebug("Сгенерирован новый контрольный код: " + cardRegistration.ConfirmationCode);
            }

            cardRegistration.Expires = DateTimeOffset.Now.AddDays(1);

            db.CardHistories.Add(new CardHistory
            {
                Id = Guid.NewGuid(),
                CardId = card.Id,
                Time = DateTimeOffset.Now,
                UserId = User.BackgroundUserId,
                Comment = $"Попытка регистрации на телефон {phone}. Отправлен код {cardRegistration.ConfirmationCode}"
            });

            await db.SaveChangesAsync();

            var msg = $"Для активации листовки *{card.Id.Substring(9)} введите проверочный код {cardRegistration.ConfirmationCode}";
            await messageService.SendAsync(phone, msg);

            return ResponseResult.Success("SMS с проверочным кодом отправлена на указанный номер телефона");
        }

        public async Task<ResponseResult> RegStep2Async(string cardNumber, PhoneNumber phone, string confirmationCode)
        {
            if (!Card.IsValidId(cardNumber))
            {
                throw new ArgumentNullException(nameof(cardNumber));
            }

            if (PhoneNumber.Empty == phone)
            {
                throw new ArgumentNullException(nameof(phone));
            }

            if (string.IsNullOrWhiteSpace(confirmationCode))
            {
                throw new ArgumentNullException(nameof(confirmationCode));
            }

            var card = await db.Cards.SingleOrDefaultAsync(x => x.Id == cardNumber);
            if (card == null)
            {
                return ResponseResult.Fail("Листовка с указанным номером не обнаружена. Проверьте правильность ввода.");
            }

            if (card.Phone == phone.ToString())
            {
                return ResponseResult.Success("Заявка на активацию листовки принята");
            }

            var existingActiveCards = await db.Cards
                .Where(x => x.Phone == phone.ToString())
                .Where(Card.ActiveCardExpression)
                .CountAsync();
            if (existingActiveCards > 0)
            {
                return ResponseResult.Fail("На ваш номер телефона уже зарегистрирована действующая листовка. Используйте первую листовку полностью, и только потом можно будет активировать ещё одну.");
            }

            if (!string.IsNullOrEmpty(card.Phone))
            {
                return ResponseResult.Fail("Листовка уже зарегистрирована на другой номер телефона. Проверьте правильность ввода номера.");
            }

            if (card.IsLocked)
            {
                return ResponseResult.Fail("Листовка не может быть зарегистрирована. Позвоните на горячую линию для уточнения деталей.");
            }

            var cardRegistrations = await db.CardRegistrations
                .Where(x => x.CardId == card.Id)
                .Where(x => x.Phone == phone.ToString())
                .Where(x => x.Expires > DateTimeOffset.Now)
                .ToListAsync();

            if (cardRegistrations.Count == 0)
            {
                return ResponseResult.Fail("Сбой процесса активации. Пожалуйста, начните активация заново.");
            }

            var cardRegistration = cardRegistrations.FirstOrDefault(x => x.ConfirmationCode == confirmationCode);
            if (cardRegistration == null)
            {
                return ResponseResult.Fail("Контрольный код указан неверно.");
            }

            card.Phone = cardRegistration.Phone;
            card.RegTime = DateTimeOffset.Now;
            await db.SaveChangesAsync();

            return ResponseResult.Success("Заявка на активацию листовки принята.");
        }
    }
}
