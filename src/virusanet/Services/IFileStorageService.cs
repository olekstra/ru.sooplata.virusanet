﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IFileStorageService
    {
        Task<string> SaveTransactionScan(Guid transactionId, string fileName, Stream file);

        Stream GetTransactionScan(Guid transactionId, string fileName);

        string[] GetAllTransactionFileNames(Guid transactionId);

        Task SaveCardScan(string cardNumber, string fileName, Stream file);

        Stream GetCardScan(string cardNumber, string fileName);
    }
}
