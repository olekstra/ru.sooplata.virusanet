﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Threading.Tasks;

    public interface IMessageService
    {
        Task SendAsync(PhoneNumber phone, string message);
    }
}
