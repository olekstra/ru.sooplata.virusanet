﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class SmscruMessageService : IMessageService
    {
        private SmscruMessageServiceOptions options;
        private ILogger logger;

        public SmscruMessageService(IOptions<SmscruMessageServiceOptions> options, ILogger<SmscruMessageService> logger)
        {
            this.options = options.Value;
            this.logger = logger;
        }

        public async Task SendAsync(PhoneNumber phone, string message)
        {
            var data = new Dictionary<string, string>();
            data["login"] = options.Login;
            data["psw"] = options.Password;
            data["sender"] = options.Sender;
            data["fmt"] = "3"; // json
            data["charset"] = "utf-8";
            data["phones"] = "7" + phone.ToOlekstraPhone();
            data["mes"] = message;

            using (var httpClient = new HttpClient())
            {
                var content = new FormUrlEncodedContent(data);
                var response = await httpClient.PostAsync(new Uri("http://smsc.ru/sys/send.php"), content);
                response.EnsureSuccessStatusCode();

                var responseText = await response.Content.ReadAsStringAsync();

                var resp = JsonConvert.DeserializeObject<SmscResponse>(responseText);

                if (resp.ErrorCode != 0)
                {
                    logger.LogDebug(responseText);
                    throw new Exception("При отправке СМС возникла ошибка");
                }
            }
        }

        private class SmscResponse
        {
            [JsonProperty("error")]
            public string Error { get; set; }

            [JsonProperty("error_code")]
            public int ErrorCode { get; set; }

            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("cnt")]
            public int SmsCount { get; set; }
        }
    }
}
