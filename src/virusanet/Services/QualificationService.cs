﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Text.RegularExpressions;
    using Microsoft.Extensions.Options;

    public class QualificationService
    {
        private BusinessRulesOptions options;

        public QualificationService(IOptions<BusinessRulesOptions> options)
        {
            this.options = options.Value;
        }

        public bool Validate(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            input = Regex.Replace(input, "\\s+", " ").Trim();

            foreach (var item in options.ValidQualificationTexts)
            {
                if (item.EndsWith("*"))
                {
                    // проверяем на "начинается с"
                    var pattern = item.Substring(0, item.Length - 1);
                    if (input.StartsWith(pattern, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return true;
                    }
                }
                else
                {
                    // проверяем на полное равенство
                    if (string.Equals(input, item, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
