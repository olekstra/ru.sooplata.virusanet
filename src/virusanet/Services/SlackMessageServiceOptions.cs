﻿namespace Olekstra.VirusaNet.Services
{
    using System;

    public class SlackMessageServiceOptions
    {
        public string WebhookUrl { get; set; }
    }
}
