﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public static class TimeZoneService
    {
        public static readonly string DefaultTimezoneId = "Russian Standard Time";

        public static TimeZoneInfo GetTimeZoneByIdOrDefault(string id)
        {
            TimeZoneInfo result;
            try
            {
                result = TimeZoneInfo.FindSystemTimeZoneById(id);
            }
            catch
            {
                result = TimeZoneInfo.FindSystemTimeZoneById(DefaultTimezoneId);
            }

            return result;
        }
    }
}
