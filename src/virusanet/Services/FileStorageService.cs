﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class FileStorageService : IFileStorageService
    {
        private ILogger logger;
        private IHostingEnvironment environment;
        private FileStorageOptions options;

        public FileStorageService(ILogger<FileStorageService> logger, IHostingEnvironment environment, IOptions<FileStorageOptions> options)
        {
            this.logger = logger;
            this.environment = environment;
            this.options = options.Value;
        }

        public async Task<string> SaveTransactionScan(Guid transactionId, string fileName, Stream file)
        {
            var uploadsFolder = Path.Combine(environment.ContentRootPath, options.TransactionFolder);

            if (!Directory.Exists(uploadsFolder))
            {
                Directory.CreateDirectory(uploadsFolder);
            }

            var transactionFolder = Path.Combine(uploadsFolder, transactionId.ToString());

            if (!Directory.Exists(transactionFolder))
            {
                Directory.CreateDirectory(transactionFolder);
            }

            var files = Directory.EnumerateFiles(transactionFolder);

            var name = transactionId.ToString();

            if (files.Count() > 0)
            {
                name += "_" + files.Count();
            }

            name += Path.GetExtension(fileName);

            var filePath = Path.Combine(transactionFolder, name);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            return name;
        }

        public Stream GetTransactionScan(Guid transactionId, string fileName)
        {
            var filePath = Path.Combine(environment.ContentRootPath, options.TransactionFolder, transactionId.ToString(), fileName);

            if (!File.Exists(filePath))
            {
                logger.LogWarning("Скан чека по пути {0} не найден", filePath);

                return null;
            }

            return File.OpenRead(filePath);
        }

        public string[] GetAllTransactionFileNames(Guid transactionId)
        {
            var folderPath = Path.Combine(environment.ContentRootPath, options.TransactionFolder, transactionId.ToString());

            string[] result = new string[] { };

            if (!Directory.Exists(folderPath))
            {
                logger.LogWarning("Папка со сканами по адрес {0} не найдена", folderPath);

                return result;
            }

            return Directory.EnumerateFiles(folderPath).Select(x => Path.GetFileName(x)).ToArray();
        }

        /// <summary>
        /// Сохранение файла скана к карте.
        /// </summary>
        /// <remarks>
        /// Уже существующий файл будет перезаписан!
        /// </remarks>
        public async Task SaveCardScan(string cardNumber, string fileName, Stream file)
        {
            var uploadsFolder = Path.Combine(environment.ContentRootPath, options.CardFolder);

            if (!Directory.Exists(uploadsFolder))
            {
                Directory.CreateDirectory(uploadsFolder);
            }

            var cardFolder = Path.Combine(uploadsFolder, cardNumber);

            if (!Directory.Exists(cardFolder))
            {
                Directory.CreateDirectory(cardFolder);
            }

            var filePath = Path.Combine(cardFolder, fileName);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
        }

        public Stream GetCardScan(string cardNumber, string fileName)
        {
            var filePath = Path.Combine(environment.ContentRootPath, options.CardFolder, cardNumber, fileName);

            if (!File.Exists(filePath))
            {
                logger.LogWarning("Скан карты по пути {0} не найден", filePath);

                return null;
            }

            return File.OpenRead(filePath);
        }
    }
}
