﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Threading.Tasks;

    public interface ICardStepMoverService
    {
        /// <summary>
        /// Проверка указанной карты, при необходимости - "продвижение" её на следующий шаг
        /// </summary>
        /// <param name="id">Код карты</param>
        /// <param name="userId">Код пользователя, инициировавшего проверку</param>
        Task CheckCardAsync(string id, Guid userId);
    }
}
