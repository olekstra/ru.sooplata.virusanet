﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;

    public class CardStepMoverService : ICardStepMoverService
    {
        private VirusaNetDb db;
        private BusinessRulesOptions options;
        private CardStepNotificationsSenderTask notificationSenderTask;
        private ILogger logger;

        public CardStepMoverService(VirusaNetDb db, IOptions<BusinessRulesOptions> options, CardStepNotificationsSenderTask notificationSenderTask, ILogger<CardStepMoverService> logger)
        {
            this.db = db;
            this.options = options.Value;
            this.notificationSenderTask = notificationSenderTask;
            this.logger = logger;
        }

        /// <summary>
        /// <see cref="ICardStepMoverService.CheckCard(string)"/>
        /// </summary>
        public async Task CheckCardAsync(string id, Guid userId)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (!Card.IsValidId(id))
            {
                throw new ArgumentException("Номер не распознан", nameof(id));
            }

            if (options.AllowedCardSteps.Length != 3)
            {
                throw new Exception("Неверные настройки: ожидается другое количество шагов");
            }

            logger.LogWarning($"Проверяем карту {id}");

            var card = await db.Cards.SingleAsync(x => x.Id == id);

            var cardOk = Card.ActiveCardFunc(card);
            if (!cardOk)
            {
                logger.LogWarning($"Карта {id} не активна (ActiveCardFunc() == false), дальше её даже не проверяем.");
                return;
            }

            var cardSteps = await db.CardSteps.AsNoTracking()
                .Where(x => x.CardId == id)
                .OrderBy(x => x.StepNumber)
                .ToListAsync();

            // если есть "открытый" шаг, то чего уж больше?
            var openStep = cardSteps.FirstOrDefault(x => x.TransactionId == null);
            if (openStep != null)
            {
                logger.LogInformation($"Карта {id} уже имеет \"открытый\" шаг {openStep.StepNumber}, разрешенный к исполнению не ранее {openStep.OpenTime}. Конец проверки");
                return;
            }

            // если шагов нет, то открываем первый
            if (cardSteps.Count == 0)
            {
                db.CardSteps.Add(new CardStep
                {
                    Id = Guid.NewGuid(),
                    CardId = id,
                    StepNumber = 1,
                    OpenTime = card.ValidationTime,
                    IsOpenNotificationSent = false,
                    TransactionId = null
                });

                db.CardHistories.Add(new CardHistory
                {
                    Id = Guid.NewGuid(),
                    CardId = id,
                    UserId = userId,
                    Time = DateTimeOffset.Now,
                    Comment = "Открыт первый шаг"
                });

                await db.SaveChangesAsync();
                if (notificationSenderTask.IsStarted)
                {
                    notificationSenderTask.TryRunImmediately();
                }

                return;
            }

            // если есть первый - пробуем открыть вротой и третий
            if (cardSteps.Count == 1)
            {
                var firstTransactionId = cardSteps.First(x => x.StepNumber == 1).TransactionId;
                var firstTransaction = await db.Transactions.AsNoTracking().SingleAsync(x => x.Id == firstTransactionId);
                if (firstTransaction.Status != TransactionStatus.Validated)
                {
                    logger.LogInformation($"Транзакция {firstTransactionId} по карте {id} еще не проверена, поэтому дальнейшие шаги еще не открываем");
                    return;
                }

                db.CardSteps.Add(new CardStep
                {
                    Id = Guid.NewGuid(),
                    CardId = id,
                    StepNumber = 2,
                    OpenTime = firstTransaction.StartTime.AddDays(options.TimeoutSinceFirstTransaction),
                    IsOpenNotificationSent = false,
                    TransactionId = null
                });

                db.CardHistories.Add(new CardHistory
                {
                    Id = Guid.NewGuid(),
                    CardId = id,
                    UserId = userId,
                    Time = DateTimeOffset.Now,
                    Comment = "Открыт второй шаг"
                });

                db.CardSteps.Add(new CardStep
                {
                    Id = Guid.NewGuid(),
                    CardId = id,
                    StepNumber = 3,
                    OpenTime = firstTransaction.StartTime.AddDays(options.TimeoutSinceFirstTransaction).AddSeconds(1),
                    IsOpenNotificationSent = false,
                    TransactionId = null
                });

                db.CardHistories.Add(new CardHistory
                {
                    Id = Guid.NewGuid(),
                    CardId = id,
                    UserId = userId,
                    Time = DateTimeOffset.Now,
                    Comment = "Открыт третий шаг"
                });

                await db.SaveChangesAsync();
                if (notificationSenderTask.IsStarted)
                {
                    notificationSenderTask.TryRunImmediately();
                }

                return;
            }

            // если шагов три, то возможно карту надо закрыть совсем
            if (cardSteps.Count == 3)
            {
                var lastTransactionId = cardSteps.First(x => x.StepNumber == 3).TransactionId;
                var lastTransaction = await db.Transactions.SingleAsync(x => x.Id == lastTransactionId);

                if (lastTransaction.Status == TransactionStatus.Validated)
                {
                    card.IsFullyUtilized = true;
                    db.CardHistories.Add(new CardHistory
                    {
                        Id = Guid.NewGuid(),
                        CardId = id,
                        UserId = userId,
                        Time = DateTimeOffset.Now,
                        Comment = "Листовка полностью использована"
                    });
                    await db.SaveChangesAsync();
                    logger.LogInformation($"Карта {id} полностью использована (третья транзакция подтверждена).");
                }
                else
                {
                    logger.LogInformation($"Транзакция {lastTransactionId} по карте {id} в статусе {lastTransaction.Status}, карту пока не закрываем.");
                }

                return;
            }

            // а если дошли сюда, то совершенно непонятно что происходит
            // возможно что-то забыли
            throw new Exception("Нарушение внутренней логики при обработке карты: нет алгоритма для текущего состояния.");
        }
    }
}
