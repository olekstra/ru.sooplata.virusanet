#!/bin/bash

dotnet build src/virusanet
dotnet test test/virusanet.tests

exitcode=$?

if [ -z "$$SLACK_BUILDSTATUS_URL" ]; then
    echo "Add SLACK_BUILDSTATUS_URL variable to enable reporting"
    exit $exitcode
fi

name=$BITBUCKET_REPO_SLUG
url=https://bitbucket.org/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/addon/pipelines/home#!/
commit_url=https://bitbucket.org/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/commits/$BITBUCKET_COMMIT
commit_hash=${BITBUCKET_COMMIT:0:8}
commit_author=`git log -1 --format=%an`
commit_timestamp=`git log -1 --format=%at`
commit_message=`git log -1 --format=%B`
status="Unknown :("
color="warning"

if (($exitcode > 0)); then
    color="danger"
    status="FAILED"
else
    color="good"
    status="succeeded"
fi

text="{
    \"attachments\": [
        {
            \"pretext\": \"Build $status\",        
            \"fallback\": \"Build $status: $name\",
            \"color\": \"$color\",
            \"title\": \"$name\",
            \"title_link\": \"$url\",
            \"text\": \"Commit <$commit_url|$commit_hash> by $commit_author:\\n$commit_message\",
            \"ts\": $commit_timestamp
        }
    ]
}"

echo
echo
echo "Reporting status to Slack..."
echo $text | curl -X POST -H 'Content-type: application/json' --data @- $SLACK_BUILDSTATUS_URL

exit $exitcode