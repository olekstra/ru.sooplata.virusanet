﻿namespace Olekstra.VirusaNet.Apteka
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Moq;
    using Services;
    using Xunit;

    public class RetailService_CompleteTransaction_Tests : IDisposable
    {
        private const string CardNumber = "1";
        private static readonly PhoneNumber PhoneNumber = new PhoneNumber("9001234567");
        private static readonly Guid DrugstoreId = Guid.NewGuid();
        private static readonly Guid TransactionId = Guid.NewGuid();

        private VirusaNetDb db;

        private RetailService service;

        public RetailService_CompleteTransaction_Tests()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<VirusaNetDb>();
            builder.UseInMemoryDatabase()
                   .UseInternalServiceProvider(serviceProvider);

            db = new VirusaNetDb(builder.Options);

            var optionsMock = new Mock<IOptions<BusinessRulesOptions>>();

            var messageMock = new Mock<IMessageService>();

            service = new RetailService(db, optionsMock.Object, messageMock.Object, serviceProvider.GetRequiredService<ILogger<RetailService>>());

            // создаем начальные (правильные) данные
            db.Cards.Add(new Card
            {
                Id = CardNumber,
                Phone = PhoneNumber.ToOlekstraPhone(),
                ValidationStatus = CardValidationStatus.Validated,
                RegTime = DateTimeOffset.Now.AddDays(-2),
                ValidationTime = DateTimeOffset.Now.AddDays(-1)
            });

            db.CardSteps.Add(new CardStep
            {
                Id = Guid.NewGuid(),
                CardId = CardNumber,
                OpenTime = DateTimeOffset.Now.AddDays(-1),
                IsOpenNotificationSent = true,
                StepNumber = 1,
                TransactionId = TransactionId
            });

            db.Drugstores.Add(new Drugstore
            {
                Id = DrugstoreId,
                Name = "Test drugstore"
            });

            db.Transactions.Add(new Transaction
            {
                Id = TransactionId,
                CardId = CardNumber,
                DrugstoreId = DrugstoreId,
                StartTime = DateTimeOffset.Now.AddMinutes(-1),
                StepNumber = 1,
                Status = TransactionStatus.Confirmed,
                ConfirmationCode = "12345"
            });

            db.SaveChanges();

            // очищаем контекст, чтобы не мешали работать с ними же
            foreach(var entity in db.ChangeTracker.Entries().ToList())
            {
                entity.State = EntityState.Detached;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        [Fact]
        public async Task WorksOk()
        {
            var userId = Guid.NewGuid();

            await service.CompleteTransactionAsync(TransactionId, userId);

            // перечитываем из базы, чтобы убедиться что сохранено там
            var trans = await db.Transactions.SingleAsync(x => x.Id == TransactionId);
            Assert.Equal(TransactionStatus.Completed, trans.Status);
        }

        [Fact]
        public async Task FailsOnWrongStatus()
        {
            // меняем статус транзакции в базе
            var trans = await db.Transactions.SingleAsync(x => x.Id == TransactionId);
            trans.Status = TransactionStatus.Aborted; // любой кроме Confirmed
            await db.SaveChangesAsync();
            db.Entry(trans).State = EntityState.Detached;

            var userId = Guid.NewGuid();
            await Assert.ThrowsAsync<Exception>(() => service.CompleteTransactionAsync(TransactionId, userId));
        }
    }
}
