﻿namespace Olekstra.VirusaNet.Apteka
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Moq;
    using Services;
    using Xunit;

    public class RetailService_BeginTransaction_Tests : IDisposable
    {
        private const string CardNumber = "1";
        private static readonly PhoneNumber PhoneNumber = new PhoneNumber("9001234567");
        private static readonly Guid DrugstoreId = Guid.NewGuid();

        private VirusaNetDb db;

        private BusinessRulesOptions options;

        private Mock<IMessageService> messageMock;

        private RetailService service;

        public RetailService_BeginTransaction_Tests()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<VirusaNetDb>();
            builder.UseInMemoryDatabase()
                   .UseInternalServiceProvider(serviceProvider);

            db = new VirusaNetDb(builder.Options);

            options = new BusinessRulesOptions();
            options.AllowedCardSteps = new[]
            {
                new BusinessRulesOptions.AllowedCardStep { Name = "Name1", Price = 150M },
                new BusinessRulesOptions.AllowedCardStep { Name = "Name2", Price = 100M },
                new BusinessRulesOptions.AllowedCardStep { Name = "Name3", Price = 50M }
            };

            var optionsMock = new Mock<IOptions<BusinessRulesOptions>>();
            optionsMock.SetupGet(x => x.Value).Returns(options);

            messageMock = new Mock<IMessageService>();

            service = new RetailService(db, optionsMock.Object, messageMock.Object, serviceProvider.GetRequiredService<ILogger<RetailService>>());

            // создаем начальные (правильные) данные
            db.Cards.Add(new Card
            {
                Id = CardNumber,
                Phone = PhoneNumber.ToOlekstraPhone(),
                ValidationStatus = CardValidationStatus.Validated,
                RegTime = DateTimeOffset.Now.AddDays(-2),
                ValidationTime = DateTimeOffset.Now.AddDays(-1)
            });

            db.CardSteps.Add(new CardStep
            {
                Id = Guid.NewGuid(),
                CardId = CardNumber,
                OpenTime = DateTimeOffset.Now.AddDays(-1),
                IsOpenNotificationSent = true,
                StepNumber = 1,
                TransactionId = null
            });

            db.Drugstores.Add(new Drugstore
            {
                Id = DrugstoreId,
                Name = "Test drugstore"
            });

            db.SaveChanges();

            // очищаем контекст, чтобы не мешали работать с ними же
            foreach(var entity in db.ChangeTracker.Entries().ToList())
            {
                entity.State = EntityState.Detached;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        [Fact]
        public async Task WorksOk()
        {
            // для начала клон теста GetNextCardStepAsync
            // чтобы убедиться что там проблем нет
            var res = await service.GetNextCardStepAsync(PhoneNumber);
            Assert.NotNull(res);
            Assert.Equal(CardNumber, res.Item1.Id);
            Assert.Equal(1, res.Item2.StepNumber);

            var userId = Guid.NewGuid();

            var msg = string.Empty;
            messageMock.Setup(x => x.SendAsync(PhoneNumber, It.IsAny<string>()))
                .Callback<PhoneNumber, string>((p, m) => { msg = m; })
                .Returns(Task.CompletedTask);

            // а теперь собственно нужный тест
            var trans = await service.BeginTransactionAsync(PhoneNumber, DrugstoreId, userId);

            Assert.NotNull(trans);

            var transId = trans.Id;

            // перечитываем из базы, чтобы убедиться что сохранено там
            trans = await db.Transactions.SingleAsync(x => x.Id == transId);

            Assert.NotNull(trans);
            Assert.Equal(DrugstoreId, trans.DrugstoreId);
            Assert.Equal(CardNumber, trans.CardId);
            Assert.Equal(TransactionStatus.Started, trans.Status);

            Assert.True(msg.Contains(trans.ConfirmationCode));
            Assert.True(msg.Contains(options.AllowedCardSteps[0].Name));

            // по идее и так всего одна, если в будущем поменяется - будем брать "правильнее"
            var transHistory = await db.TransactionHistories.SingleAsync();
            Assert.Equal(trans.Id, transHistory.TransactionId);
            Assert.Equal(userId, transHistory.UserId);

            // по идее и так всего одна, если в будущем поменяется - будем брать "правильнее"
            var cardStep = await db.CardSteps.SingleAsync();
            Assert.Equal(trans.Id, cardStep.TransactionId);
        }
    }
}
