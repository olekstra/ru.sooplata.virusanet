﻿namespace Olekstra.VirusaNet.Apteka
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Moq;
    using Services;
    using Xunit;

    public class RetailService_GetNextCardStep_Tests : IDisposable
    {
        private const string CardNumber = "1";
        private static readonly PhoneNumber PhoneNumber = new PhoneNumber("9001234567");

        private VirusaNetDb db;

        private BusinessRulesOptions options;

        private RetailService service;

        public RetailService_GetNextCardStep_Tests()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<VirusaNetDb>();
            builder.UseInMemoryDatabase()
                   .UseInternalServiceProvider(serviceProvider);

            db = new VirusaNetDb(builder.Options);

            options = new BusinessRulesOptions();
            options.AllowedCardSteps = new[]
            {
                new BusinessRulesOptions.AllowedCardStep { Name = "Name1", Price = 150M },
                new BusinessRulesOptions.AllowedCardStep { Name = "Name1", Price = 100M },
                new BusinessRulesOptions.AllowedCardStep { Name = "Name1", Price = 50M }
            };

            var optionsMock = new Mock<IOptions<BusinessRulesOptions>>();
            optionsMock.SetupGet(x => x.Value).Returns(options);

            var messageMock = new Mock<IMessageService>();

            service = new RetailService(db, optionsMock.Object, messageMock.Object, serviceProvider.GetRequiredService<ILogger<RetailService>>());

            // создаем начальные (правильные) данные
            db.Cards.Add(new Card
            {
                Id = CardNumber,
                Phone = PhoneNumber.ToString(),
                ValidationStatus = CardValidationStatus.Validated,
                RegTime = DateTimeOffset.Now.AddDays(-2),
                ValidationTime = DateTimeOffset.Now.AddDays(-1)
            });

            db.CardSteps.Add(new CardStep
            {
                Id = Guid.NewGuid(),
                CardId = CardNumber,
                OpenTime = DateTimeOffset.Now.AddDays(-1),
                IsOpenNotificationSent = true,
                StepNumber = 1,
                TransactionId = null
            });

            db.SaveChanges();

            // очищаем контекст, чтобы не мешали работать с ними же
            foreach (var entity in db.ChangeTracker.Entries().ToList())
            {
                entity.State = EntityState.Detached;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        [Fact]
        public async Task WorksOk()
        {
            // все данные уже подготовлены, просто вызываем

            var res = await service.GetNextCardStepAsync(PhoneNumber);

            Assert.NotNull(res);
            Assert.Equal(CardNumber, res.Item1.Id);
            Assert.Equal(1, res.Item2.StepNumber);
        }

        [Fact]
        public async Task IgnoresNotVerifiedCards()
        {
            var card = db.Cards.Single(x => x.Id == CardNumber);

            card.ValidationStatus = CardValidationStatus.Rejected;
            await db.SaveChangesAsync();
        
            var res1 = await service.GetNextCardStepAsync(PhoneNumber);
            Assert.Null(res1);


            card.ValidationStatus = CardValidationStatus.Unknown;
            await db.SaveChangesAsync();

            var res2 = await service.GetNextCardStepAsync(PhoneNumber);
            Assert.Null(res2);
        }

        [Fact]
        public async Task IgnoresLockedCards()
        {
            var card = db.Cards.Single(x => x.Id == CardNumber);

            card.IsLocked = true;
            await db.SaveChangesAsync();

            var res = await service.GetNextCardStepAsync(PhoneNumber);
            Assert.Null(res);
        }

        [Fact]
        public async Task IgnoresFullyUtilizedCard()
        {
            var card = db.Cards.Single(x => x.Id == CardNumber);

            card.IsFullyUtilized = true;
            await db.SaveChangesAsync();

            var res = await service.GetNextCardStepAsync(PhoneNumber);
            Assert.Null(res);
        }

        [Fact]
        public async Task ChecksStepOpenTime()
        {
            var cardStep = db.CardSteps.Single(x => x.CardId == CardNumber);

            cardStep.OpenTime = DateTimeOffset.Now.AddDays(1);
            await db.SaveChangesAsync();

            var res = await service.GetNextCardStepAsync(PhoneNumber);
            Assert.Null(res);
        }

        [Fact]
        public async Task IgnoresStepWithTransaction()
        {
            var cardStep = db.CardSteps.Single(x => x.CardId == CardNumber);

            cardStep.TransactionId = Guid.NewGuid();
            await db.SaveChangesAsync();

            var res = await service.GetNextCardStepAsync(PhoneNumber);
            Assert.Null(res);
        }
    }
}
