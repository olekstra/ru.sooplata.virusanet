﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Moq;
    using Services;
    using Xunit;

    public class CardStepNotificationsSenderTask_Tests : IDisposable
    {
        private const string CardNumber = "1";
        private static readonly PhoneNumber PhoneNumber = new PhoneNumber("9001234567");
        private static readonly Guid DrugstoreId = Guid.NewGuid();
        private static readonly Guid TransactionId = Guid.NewGuid();

        private Mock<IMessageService> messageMock;
        private BusinessRulesOptions options;
        private VirusaNetDb db;

        private CardStepNotificationsSenderTask_TestSample service;

        public CardStepNotificationsSenderTask_Tests()
        {
            messageMock = new Mock<IMessageService>();

            options = new BusinessRulesOptions();
            var optionsMock = new Mock<IOptions<BusinessRulesOptions>>();
            optionsMock.SetupGet(x => x.Value).Returns(options);

            var serviceCollection = new ServiceCollection()
                .AddLogging()
                .AddEntityFrameworkInMemoryDatabase()
                .AddSingleton(messageMock.Object)
                .AddSingleton(optionsMock.Object);

            var builder = new DbContextOptionsBuilder<VirusaNetDb>();
            builder.UseInMemoryDatabase()
                   .UseInternalServiceProvider(serviceCollection.BuildServiceProvider());

            db = new VirusaNetDb(builder.Options);

            serviceCollection.AddSingleton(db);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            service = new CardStepNotificationsSenderTask_TestSample(
                serviceProvider, 
                serviceProvider.GetRequiredService<ILoggerFactory>(),
                serviceProvider.GetRequiredService<IServiceScopeFactory>());

            options.AllowedCardSteps = new BusinessRulesOptions.AllowedCardStep[] 
            {
                new BusinessRulesOptions.AllowedCardStep { Name = "test name", Price = 11 }
            };

            // создаем начальные (правильные) данные
            db.Cards.Add(new Card
            {
                Id = CardNumber,
                Phone = PhoneNumber.ToOlekstraPhone(),
                ValidationStatus = CardValidationStatus.Validated,
                RegTime = DateTimeOffset.Now.AddDays(-2),
                ValidationTime = DateTimeOffset.Now.AddDays(-1)
            });

            db.CardSteps.Add(new CardStep
            {
                Id = Guid.NewGuid(),
                CardId = CardNumber,
                OpenTime = DateTimeOffset.Now.AddDays(-1),
                IsOpenNotificationSent = false,
                StepNumber = 1,
                TransactionId = null
            });

            db.SaveChanges();

            // очищаем контекст, чтобы не мешали работать с ними же
            foreach (var entity in db.ChangeTracker.Entries().ToList())
            {
                entity.State = EntityState.Detached;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        [Fact]
        public void WorksOk()
        {
            service.Run();

            var cardStep = db.CardSteps.Single();
            Assert.True(cardStep.IsOpenNotificationSent);

            messageMock.Verify(x => x.SendAsync(PhoneNumber, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void IgnoresAlreadySent()
        {
            var cardStep = db.CardSteps.Single();
            cardStep.IsOpenNotificationSent = true;
            db.SaveChanges();
            db.Entry(cardStep).State = EntityState.Detached;

            service.Run();

            messageMock.Verify(x => x.SendAsync(PhoneNumber, It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void IgnoresWithTimeInFuture()
        {
            var cardStep = db.CardSteps.Single();
            cardStep.OpenTime = DateTimeOffset.Now.AddHours(1);
            db.SaveChanges();
            db.Entry(cardStep).State = EntityState.Detached;

            service.Run();

            messageMock.Verify(x => x.SendAsync(PhoneNumber, It.IsAny<string>()), Times.Never);
        }
    }
}
