﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using Microsoft.Extensions.Options;
    using Moq;
    using Xunit;

    public class QualificationService_Validate_Tests
    {
        private BusinessRulesOptions options;

        private QualificationService service;

        public QualificationService_Validate_Tests()
        {
            options = new BusinessRulesOptions();
            options.ValidQualificationTexts = new[]
            {
                "Слово",
                "Два слова",
                "Начинается с*",
                "Три слова *"
            };

            var optionsMock = new Mock<IOptions<BusinessRulesOptions>>();
            optionsMock.SetupGet(x => x.Value).Returns(options);

            service = new QualificationService(optionsMock.Object);

            Console.OutputEncoding = System.Text.Encoding.UTF8;
        }

        [Theory]
        [InlineData("Слово")]
        [InlineData("СлОвО")]
        [InlineData("ДВА слоВА")]
        [InlineData("  ДВА слоВА  ")]               // концевые пробелы должны обрезаться
        [InlineData("Начинается с чего-то")]
        [InlineData("  Начинается   с  чего-то  ")] // несколько пробелов должны заменяться на один
        [InlineData("Начинается смотри")]           // спорно, но формально же "начинается"!
        [InlineData("Три слова и дальше")]
        public void ValidateSuccess(string input)
        {
            Assert.True(service.Validate(input));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("    ")]
        [InlineData("дело")]            // нет такого варианта вообще
        [InlineData("слово слово")]     // там не было "начинается с"
        [InlineData("Начинается не")]   // "не" не равно "с"
        [InlineData("Три словатут")]    // пробела нет после "слова"
        public void ValidateFail(string input)
        {
            Assert.False(service.Validate(input));
        }
    }
}
