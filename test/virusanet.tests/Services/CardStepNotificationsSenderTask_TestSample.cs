﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using RecurrentTasks;

    public class CardStepNotificationsSenderTask_TestSample : CardStepNotificationsSenderTask
    {
        private IServiceProvider serviceProvider;

        public CardStepNotificationsSenderTask_TestSample(IServiceProvider serviceProvider, ILoggerFactory loggerFactory, IServiceScopeFactory serviceScopeFactory) 
            : base(loggerFactory, serviceScopeFactory)
        {
            this.serviceProvider = serviceProvider;
        }

        public void Run()
        {
            base.Run(serviceProvider, new TaskRunStatus());
        }
    }
}
