﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Model;
    using Moq;
    using Services;
    using Xunit;

    public class CardStepMoverService_CheckCard_Tests : IDisposable
    {
        private const string CardNumber = "1234567890123";
        private static readonly PhoneNumber PhoneNumber = new PhoneNumber("9001234567");
        private static readonly Guid DrugstoreId = Guid.NewGuid();
        private static readonly Guid TransactionId = Guid.NewGuid();
        private static readonly Guid UserId = Guid.NewGuid();

        private Mock<IMessageService> messageMock;
        private BusinessRulesOptions options;
        private VirusaNetDb db;

        private CardStepMoverService service;

        public CardStepMoverService_CheckCard_Tests()
        {
            messageMock = new Mock<IMessageService>();

            options = new BusinessRulesOptions();
            var optionsMock = new Mock<IOptions<BusinessRulesOptions>>();
            optionsMock.SetupGet(x => x.Value).Returns(options);

            var efServiceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddIdentity<User, UserRole>().AddUserStore<VirusaNetDb>().Services
                .AddEntityFrameworkInMemoryDatabase()
                .AddSingleton(messageMock.Object)
                .AddSingleton(optionsMock.Object)
                .AddDbContext<VirusaNetDb>(x => x.UseInMemoryDatabase().UseInternalServiceProvider(efServiceProvider))
                .AddSingleton<CardStepNotificationsSenderTask>()
                .AddTransient<CardStepMoverService>()
                .BuildServiceProvider();

            db = serviceProvider.GetRequiredService<VirusaNetDb>();

            service = serviceProvider.GetRequiredService<CardStepMoverService>();

            options.AllowedCardSteps = new BusinessRulesOptions.AllowedCardStep[]
            {
                new BusinessRulesOptions.AllowedCardStep { Name = "test name 1", Price = 11 },
                new BusinessRulesOptions.AllowedCardStep { Name = "test name 2", Price = 12 },
                new BusinessRulesOptions.AllowedCardStep { Name = "test name 3", Price = 13 }
            };
            options.TimeoutSinceFirstTransaction = 10;

            // создаем начальные (правильные) данные
            db.Cards.Add(new Card
            {
                Id = CardNumber,
                Phone = PhoneNumber.ToOlekstraPhone(),
                ValidationStatus = CardValidationStatus.Validated,
                RegTime = DateTimeOffset.Now.AddDays(-2),
                ValidationTime = DateTimeOffset.Now.AddDays(-1)
            });

            db.SaveChanges();

            // очищаем контекст, чтобы не мешали работать с ними же
            foreach (var entity in db.ChangeTracker.Entries().ToList())
            {
                entity.State = EntityState.Detached;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        [Fact]
        public async Task CreatesFirstStep()
        {
            await service.CheckCardAsync(CardNumber, UserId);

            // должен был появиться первый шаг и запись в истории
            var cardStep = await db.CardSteps.SingleAsync();
            Assert.Equal(CardNumber, cardStep.CardId);
            Assert.Equal(1, cardStep.StepNumber);
            Assert.Null(cardStep.TransactionId);

            var cardHistory = await db.CardHistories.SingleAsync();
            Assert.Equal(CardNumber, cardHistory.CardId);
        }

        [Fact]
        public async Task IgnoresInactiveCard()
        {
            // отключаем карту
            var card = await db.Cards.SingleAsync();
            card.IsLocked = true;
            await db.SaveChangesAsync();
            db.Entry(card).State = EntityState.Detached;

            // поехали
            await service.CheckCardAsync(CardNumber, UserId);

            // ничего не должно был добавиться
            Assert.Equal(0, await db.CardSteps.CountAsync());
            Assert.Equal(0, await db.CardHistories.CountAsync());
        }

        [Fact]
        public async Task DoesNothingIfStepAlreadyOpen()
        {
            // добавляем шаг который уже открыт
            var cardStep = new CardStep
            {
                Id = Guid.NewGuid(),
                CardId = CardNumber,
                StepNumber = 1,
                OpenTime = DateTimeOffset.Now.AddHours(-1),
            };
            db.CardSteps.Add(cardStep);
            await db.SaveChangesAsync();
            db.Entry(cardStep).State = EntityState.Detached;

            // поехали
            await service.CheckCardAsync(CardNumber, UserId);

            // ничего не должно было измениться
            Assert.Equal(1, await db.CardSteps.CountAsync()); // existing one
            Assert.Equal(0, await db.CardHistories.CountAsync());
        }

        [Fact]
        public async Task DowsNothingIfTransactionNotValidated()
        {
            // добавляем шаг и по нему непроверенную транзакцию
            var cardStep = new CardStep
            {
                Id = Guid.NewGuid(),
                CardId = CardNumber,
                StepNumber = 1,
                OpenTime = DateTimeOffset.Now.AddHours(-1),
                TransactionId = TransactionId
            };
            db.CardSteps.Add(cardStep);

            var trans = new Transaction
            {
                Id = TransactionId,
                CardId = CardNumber,
                StepNumber = 1,
                DrugstoreId = DrugstoreId,
                Status = TransactionStatus.AwaitingValidation,
                ConfirmationCode = "123456",
                StartTime = DateTimeOffset.Now.AddHours(-1)
            };
            db.Transactions.Add(trans);

            await db.SaveChangesAsync();

            db.Entry(trans).State = EntityState.Detached;
            db.Entry(cardStep).State = EntityState.Detached;

            // поехали
            await service.CheckCardAsync(CardNumber, UserId);

            // ничего не должно было добавиться
            Assert.Equal(1, await db.CardSteps.CountAsync()); // existing one
            Assert.Equal(0, await db.CardHistories.CountAsync());
        }

        [Fact]
        public async Task CreatesSteps2and3()
        {
            // добавляем шаг и по нему проверенную транзакцию
            var cardStep = new CardStep
            {
                Id = Guid.NewGuid(),
                CardId = CardNumber,
                StepNumber = 1,
                OpenTime = DateTimeOffset.Now.AddHours(-1),
                TransactionId = TransactionId
            };
            db.CardSteps.Add(cardStep);

            var trans = new Transaction
            {
                Id = TransactionId,
                CardId = CardNumber,
                StepNumber = 1,
                DrugstoreId = DrugstoreId,
                Status = TransactionStatus.Validated,
                ConfirmationCode = "123456",
                StartTime = DateTimeOffset.Now.AddHours(-1)
            };
            db.Transactions.Add(trans);

            await db.SaveChangesAsync();

            db.Entry(trans).State = EntityState.Detached;
            db.Entry(cardStep).State = EntityState.Detached;

            // поехали
            await service.CheckCardAsync(CardNumber, UserId);

            // должно добавиться два шага и две истории
            Assert.Equal(3, await db.CardSteps.CountAsync());
            var cardStep2 = await db.CardSteps.SingleAsync(x => x.CardId == CardNumber && x.StepNumber == 2);
            var cardStep3 = await db.CardSteps.SingleAsync(x => x.CardId == CardNumber && x.StepNumber == 3);
            Assert.NotNull(cardStep2);
            Assert.NotNull(cardStep3);

            // Check issue #25
            Assert.True(cardStep3.OpenTime > cardStep2.OpenTime); 

            Assert.Equal(2, await db.CardHistories.CountAsync());
        }

        [Fact]
        public async Task DoesNothingIfAll3StepsExist()
        {
            // добавляем три шага
            var cardSteps = new byte[] { 1, 2, 3 }
                .Select(x => new CardStep
                {
                    Id = Guid.NewGuid(),
                    CardId = CardNumber,
                    StepNumber = x,
                    OpenTime = DateTimeOffset.Now.AddHours(-4 + x),
                    TransactionId = x == 1 ? TransactionId : (Guid?)null
                });
            db.CardSteps.AddRange(cardSteps);

            var trans = new Transaction
            {
                Id = TransactionId,
                CardId = CardNumber,
                StepNumber = 1,
                DrugstoreId = DrugstoreId,
                Status = TransactionStatus.Validated,
                ConfirmationCode = "123456",
                StartTime = DateTimeOffset.Now.AddHours(-1)
            };
            db.Transactions.Add(trans);

            await db.SaveChangesAsync();

            db.Entry(trans).State = EntityState.Detached;
            foreach (var cardStep in cardSteps)
            {
                db.Entry(cardStep).State = EntityState.Detached;
            }

            // поехали
            await service.CheckCardAsync(CardNumber, UserId);

            // ничего не должно измениться
            Assert.Equal(3, await db.CardSteps.CountAsync());
            Assert.Equal(0, await db.CardHistories.CountAsync());
        }

        [Fact]
        public async Task CloseCardIfThirdTransactionValidated()
        {
            // добавляем три шага
            var cardSteps = new byte[] { 1, 2, 3 }
                .Select(x => new CardStep
                {
                    Id = Guid.NewGuid(),
                    CardId = CardNumber,
                    StepNumber = x,
                    OpenTime = DateTimeOffset.Now.AddHours(-4 + x),
                    TransactionId = TransactionId //HACK: всё мэппим на одну транзакцию. плохо, но не смертельно
                });
            db.CardSteps.AddRange(cardSteps);

            var trans = new Transaction
            {
                Id = TransactionId,
                CardId = CardNumber,
                StepNumber = 1,
                DrugstoreId = DrugstoreId,
                Status = TransactionStatus.Validated,
                ConfirmationCode = "123456",
                StartTime = DateTimeOffset.Now.AddHours(-1)
            };
            db.Transactions.Add(trans);

            await db.SaveChangesAsync();

            db.Entry(trans).State = EntityState.Detached;
            foreach (var cardStep in cardSteps)
            {
                db.Entry(cardStep).State = EntityState.Detached;
            }

            // поехали
            await service.CheckCardAsync(CardNumber, UserId);

            // карта должна закрыться
            Assert.True(db.Cards.Single().IsFullyUtilized);
        }
    }
}
