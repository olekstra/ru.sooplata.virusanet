﻿namespace Olekstra.VirusaNet.Services
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Moq;
    using Xunit;

    public class SmscruMessageServiceTests
    {
        private SmscruMessageServiceOptions options;

        private SmscruMessageService service;

        public SmscruMessageServiceTests()
        {
            options = new SmscruMessageServiceOptions
            {
                Login = "",
                Password = "",
                Sender = "sooplata.ru"
            };

            var optionsMock = new Mock<IOptions<SmscruMessageServiceOptions>>();
            optionsMock.SetupGet(x => x.Value).Returns(options);

            service = new SmscruMessageService(optionsMock.Object, new Mock<ILogger<SmscruMessageService>>().Object);
        }

        [Fact(Skip = "Need Login/Password for this test to run")]
        public async Task Send()
        {
            await service.SendAsync(new PhoneNumber("000 000-00-00"), "проверка связи");
        }
    }
}
