﻿namespace Olekstra.VirusaNet.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Xunit;

    public class VirusaNetDb_Identity_Claims
    {
        private IUserClaimStore<User> db;

        public VirusaNetDb_Identity_Claims()
        {
            db = new VirusaNetDb(new DbContextOptions<VirusaNetDb>()) as IUserClaimStore<User>;
        }

        [Fact]
        public async Task DrugstoreUser_HasSingleClaim_WithDrugstoreIdValue()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                DrugstoreId = Guid.NewGuid()
            };

            var result = await db.GetClaimsAsync(user, CancellationToken.None);

            Assert.Equal(1, result.Count);

            Assert.Equal("DrugstoreId", result[0].Type);
            Assert.Equal(user.DrugstoreId.ToString(), result[0].Value);
        }

        [Fact]
        public async Task DrugstoreUser_HasNoAdminClaim()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                DrugstoreId = Guid.NewGuid(),
                IsAdmin = true
            };

            var result = await db.GetClaimsAsync(user, CancellationToken.None);

            // никаких IsAdmin!
            // раз есть DrugstoreId, то всё остальное должно отсутсвовать!
            Assert.Equal(1, result.Count);
            Assert.Equal("DrugstoreId", result[0].Type);
        }

        [Fact]
        public async Task Admin_HasTwoClaims()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                IsAdmin = true
            };

            var result = await db.GetClaimsAsync(user, CancellationToken.None);

            Assert.Equal(2, result.Count);

            var claimAdmin = result.FirstOrDefault(x => x.Type == "IsAdmin");
            Assert.NotNull(claimAdmin);
            Assert.Equal("true", claimAdmin.Value);

            var claimManager = result.FirstOrDefault(x => x.Type == "IsManager");
            Assert.NotNull(claimManager);
            Assert.Equal("true", claimManager.Value);
        }

        [Fact]
        public async Task Manager_HasOneClaim()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                IsAdmin = false
            };

            var result = await db.GetClaimsAsync(user, CancellationToken.None);

            Assert.Equal(1, result.Count);

            var claimManager = result.FirstOrDefault(x => x.Type == "IsManager");
            Assert.NotNull(claimManager);
            Assert.Equal("true", claimManager.Value);
        }
    }
}
