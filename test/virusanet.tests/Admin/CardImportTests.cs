﻿namespace Olekstra.VirusaNet.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Controllers;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Model;
    using Moq;
    using Xunit;

    public class CardImportTests : IDisposable
    {
        private static readonly Guid UserId = Guid.NewGuid();

        private VirusaNetDb db;
        private HttpContext httpContext;
        private CardImport controller;

        public CardImportTests()
        {
            var efServiceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            httpContext = new DefaultHttpContext();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddIdentity<User, UserRole>().AddUserStore<VirusaNetDb>().Services
                .AddEntityFrameworkInMemoryDatabase()
                .AddDbContext<VirusaNetDb>(x => x.UseInMemoryDatabase().UseInternalServiceProvider(efServiceProvider))
                .AddTransient<CardImport>()
                .AddSingleton<IHttpContextAccessor>(new HttpContextAccessor { HttpContext = httpContext })
                .BuildServiceProvider();

            db = serviceProvider.GetRequiredService<VirusaNetDb>();

            controller = serviceProvider.GetRequiredService<CardImport>();

            var urlMock = new Mock<IUrlHelper>(MockBehavior.Loose);
            urlMock.SetReturnsDefault("/");

            controller.Url = urlMock.Object;
            controller.ControllerContext.HttpContext = httpContext;

            var claims = new List<Claim> { new Claim(ClaimTypes.NameIdentifier, UserId.ToString()) };
            httpContext.User = new ClaimsPrincipal(new ClaimsIdentity(claims));
        }

        public void Dispose()
        {
            db.Dispose();
        }

        [Fact]
        public async Task NonValidNumbersNotAccepted()
        {
            var numbers = new[] { "1234567890123", "12345", "23456", "1234567890234" };
            var numbersString = string.Join(Environment.NewLine, numbers);

            var result = await controller.Import(numbersString);

            Assert.IsType<ViewResult>(result);

            var viewResult = result as ViewResult;
            Assert.False(viewResult.ViewData.ModelState.IsValid);
            Assert.Equal(numbersString, viewResult.Model);

            // проверяем что ничего не добавилось в базу
            Assert.Equal(0, db.Cards.Count());
        }

        [Fact]
        public async Task ExistingNumbersNotAccepted()
        {
            var numbers = new[] { "1234567890123", "1234567890234" };
            var numbersString = string.Join(Environment.NewLine, numbers);

            db.Cards.Add(new Card { Id = numbers[1] });
            await db.SaveChangesAsync();

            var result = await controller.Import(numbersString);

            Assert.IsType<ViewResult>(result);

            var viewResult = result as ViewResult;
            Assert.False(viewResult.ViewData.ModelState.IsValid);
            Assert.Equal(numbersString, viewResult.Model);

            // проверяем что ничего не добавилось в базу (одну мы добавили сами)
            Assert.Equal(1, db.Cards.Count());
        }


        [Fact]
        public async Task WorksOk()
        {
            var existingId = "1234567890777";

            var numbers = new[] { "1234567890123", "1234567890234" };
            var numbersString = string.Join(Environment.NewLine, numbers);

            db.Cards.Add(new Card { Id = existingId });
            await db.SaveChangesAsync();

            var result = await controller.Import(numbersString);
            Assert.IsType<RedirectResult>(result);

            // проверяем, что в базе теперь все три объекта
            var list = await db.Cards.ToListAsync();
            Assert.Equal(3, list.Count);
            Assert.NotNull(list.Find(x => x.Id == existingId));
            Assert.NotNull(list.Find(x => x.Id == numbers[0]));
            Assert.NotNull(list.Find(x => x.Id == numbers[1]));
        }
    }
}
