﻿namespace Olekstra.VirusaNet.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Controllers;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Model;
    using Moq;
    using Services;
    using ViewModels;
    using Xunit;

    public class CardsTests : IDisposable
    {
        private VirusaNetDb db;
        private Cards controller;

        public CardsTests()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<VirusaNetDb>();
            builder.UseInMemoryDatabase()
                   .UseInternalServiceProvider(serviceProvider);

            db = new VirusaNetDb(builder.Options);

            controller = new Cards(db, new Mock<IFileStorageService>().Object, serviceProvider.GetRequiredService<ILogger<Cards>>());
        }

        public void Dispose()
        {
            db.Dispose();
        }

        [Fact]
        public async Task ReturnModelOnWrongInput()
        {
            var result = await controller.Details("not-a-number");

            Assert.IsType<ViewResult>(result);

            var viewResult = result as ViewResult;
            Assert.NotNull(viewResult.Model);
            Assert.IsType<CardDetailsViewModel>(viewResult.Model);

            var model = (CardDetailsViewModel)viewResult.Model;
            Assert.False(string.IsNullOrWhiteSpace(model.Message));
        }

        [Fact]
        public async Task ReturnModelOnNonExistingCard()
        {
            var result = await controller.Details("1234567890123");

            Assert.IsType<ViewResult>(result);

            var viewResult = result as ViewResult;
            Assert.NotNull(viewResult.Model);
            Assert.IsType<CardDetailsViewModel>(viewResult.Model);

            var model = (CardDetailsViewModel)viewResult.Model;
            Assert.False(string.IsNullOrWhiteSpace(model.Message));
        }

        [Fact]
        public async Task WorksOk()
        {
            var cardId = "1234567890123";

            db.Cards.Add(new Card { Id = cardId });
            db.CardSteps.Add(new CardStep { CardId = cardId, StepNumber = 1 });
            db.Transactions.Add(new Transaction { Id = Guid.NewGuid(), CardId = cardId });
            await db.SaveChangesAsync();

            var result = await controller.Details(cardId);

            Assert.IsType<ViewResult>(result);

            var viewResult = result as ViewResult;
            Assert.NotNull(viewResult.Model);
            Assert.IsType<CardDetailsViewModel>(viewResult.Model);

            var model = (CardDetailsViewModel)viewResult.Model;
            Assert.True(string.IsNullOrWhiteSpace(model.Message));
            Assert.NotNull(model.Card);
            Assert.NotNull(model.CardSteps);
            Assert.Equal(1, model.CardSteps.Count);
            Assert.NotNull(model.Transactions);
            Assert.Equal(1, model.Transactions.Count);
        }
    }
}
